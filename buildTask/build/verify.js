function verify(arg, callBack) {
	console.log("参数 = " + arg);
	if (!arg) {
		callBack(false, "参数无效！1");
		return;
	};
	var msg = arg.msg;
	console.log(arg.msg);
	if (!msg) {
		callBack(false, "参数无效！2");
		return;
	}
	var msgJson = JSON.parse(msg);
	var taskId = msgJson.task_id; //任务id
	var identifier = msgJson.identifier; //项目identifier
	var version = msgJson.version; //应用版本
	var build = msgJson.build; //应用构建号
	var appkey = msgJson.appkey; //应用appkey
	var certificate = msgJson.certificate; //证书文件
	var secretKey = msgJson.password; //证书密匙
	var provision = msgJson.provision; //证书文件 IOS
	if (!identifier) {
		callBack(false, "参数无效！3");
		return;
	};
	var tel = /^[a-zA-Z]{1}([a-zA-Z0-9]+[.])+[a-zA-Z]{1}([A-z0-9]+)$/;
	var flag = tel.test(identifier);
	console.log("identifier ====================" +identifier);
	if (!flag) {
		callBack(false, "参数无效！identify无效");
		return;
	};
	if (!certificate || !secretKey || !provision) {
		callBack(false, "请上传证书！");
		return;
	};
	callBack(true, "参数验证成功！");
	return;
}


exports.verify = verify;