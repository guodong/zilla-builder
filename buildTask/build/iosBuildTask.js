var fs = require('fs');
var url = require('url');
var http = require('http');
var path = require('path');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var file = require('./file');
var copyFile = require('./copyFile');
var verify = require('./verify');
var db = require('../models/db');
var Task = require('../models/buildTask');
var process = require('./process');
var async = require('async');
/*
	ios 打包任务

	source  源码对象：   包括：type url username password   
	type{ 1. http 源码包下载 2. svn  源代码下载 3. github 源代码下载 4. 使用变色龙代码最新代码打包}

	downImgFiles 下载文件对象 包括：
		icon57 icon114 icon120  icon76  icon152
		default480 default960 default1136 default1024 default2048
	downUserFiles 用户信息对象  包括：
	userP12  : 密匙
	password : 密码
	provision : 证书文件 

	task 对象  任务对象

	callback 回调信息
*/

var logInfo = '';

function startBuild(source, downImgFiles, downFile, task_id, identifier, task, msg, shameName, callback) {

	// var taskSourcePath = path.resolve('source', 'ios');
	var taskSourcePath = path.resolve('source', 'ios');
	var taskPath = path.resolve('Task', task_id + identifier);
	console.log('taskPath' + taskPath);
	var processPath = path.resolve('process', 'process.sh');
	var taskProcessPath = path.resolve('Task', task_id + identifier, 'process.sh');
	console.log("task =  " + task);
	async.auto({
			updateDb: function(cb) {
				task.statue = '2';
				Task.update(task, function(error, bars) {
					if (error) {
						console.log("服务器连接失败" + error);
					}
					cb();
				});

			},
			/*
				1. ios 代码 准备 

				TODO: 支持远程代码打包服务:  
					a. http 源码包下载
					b. svn  源代码下载
					c. github 源代码下载
					d. 使用变色龙代码最新代码打包
			*/
			//拷贝源码文件
			copySourceFile: ['updateDb',
				function(cb) {
					copyFile.copyDir(taskSourcePath, taskPath + '/ios', function() {
						console.log('文件源代码拷贝');
						cb();
					});
				}
			],
			//拷贝命令行文件
			copyProcessFile: ['copySourceFile',
				function(cb) {
					copyFile.copyFile(processPath, taskProcessPath, function() {
						console.log('命令行拷贝');
						cb();
					});
				}
			],
			createConfigFile: ['copyProcessFile',
				function(cb) {
					var filedate = '[app]\n';
					var identifier = 'identifier=' + task.identifier + "\n";
					var version = 'version=' + task.version + "\n";
					var build = 'build=' + task.build + "\n";
					var projectName = 'projectName=' + msg.appName + '\n';
					var appkey = 'appkey=' + msg.appkey + "\n";
					var secret = 'secret=' + msg.secret + "\n";
					var schemeName = 'schemeName=Chamleon-template\n';
					var isWorkspace = 'isWorkspace=True\n';
					var password = 'password=123456\n';
					var ipaFilePath = 'ipaFilePath=' + taskPath + '/OutIPAs/' + shameName + '.ipa\n';
					var mobileprovisionPath = 'mobileprovisionPath=mobileprovision/mobileprovision.mobileprovision\n';
					var P12PathFile = 'P12PathFile=p12/p12.p12';
					var workspaceName = 'workspaceName=Chamleon-template\n';

					filedate += identifier;
					filedate += version;
					filedate += build;
					filedate += projectName;
					filedate += appkey;
					filedate += secret;
					filedate += schemeName;
					filedate += isWorkspace;
					filedate += password;
					filedate += ipaFilePath;
					filedate += mobileprovisionPath;
					filedate += workspaceName;
					filedate += P12PathFile;


					console.log("filedate = " + filedate);
					file.saveFile(taskPath + "/config.ini", filedate, function(err) {
						cb();
					});
				}
			],
			/*
			2.文件下载 
			下载文件 包括：icon  DefaultPage  cer  证书
		*/
			//下载资源图片
			downImg: ['createConfigFile',
				function(cb) {
					console.log('下载图片');
					if (downImgFiles && downImgFiles.length > 0) {
						async.forEach(downImgFiles, function(item, callback1) {
							var json = JSON.parse(item);
							var file_url = json.url;
							file.downFile(file_url, taskPath + '/ios/Resource/' + json.key + '.png', function() {
								callback1();
							});
						}, function(err) {
							cb();
						});
					} else {
						cb();
					};
				}
			],
			//下载
			downUserFile: ['createConfigFile',
				function(cb) {
					if (downFile && downFile.length > 0) {
						async.forEach(downFile, function(item, callback1) {
							console.log("item ==" + item);
							var json = JSON.parse(item);
							var file_url = json.url;
							file.downFile(file_url, taskPath + '/' + json.key + '/' + json.key + '.' + json.key, function() {
								callback1();
							});
						}, function(err) {
							cb();
						});
					} else {
						cb();
					};
				}
			],
			/*3.开始执行打包命令
			更新数据库数据
		*/
			runProcess: ['downImg', 'downUserFile',
				function(cb) {
					var logPath = path.resolve('Log', 'log_' + task_id + identifier + '.log');
					var ipaPath = taskPath + '/OutIPAs/' + shameName + '.ipa';
					fs.mkdirSync(path.dirname(ipaPath));
					process.startExec(taskProcessPath, logPath, ipaPath, '', function(statue, error, buildInfo) {
						if (!statue && error) {
							console.log(error);
						}
						logInfo = buildInfo;
						cb();
					});
				}
			]
		},
		function(error, results) {
			if (error) {
				callback(false, task);
				return;
			} else {
				//判断ipa是否存在
				var ipaPath = taskPath + '/OutIPAs/' + shameName + '.ipa';

				fs.exists(ipaPath, function(exists) {
					console.log("ipaPath = " + ipaPath);
					console.log("file  exists" + exists);
					var logPath = path.resolve('Log', 'log_' + task_id + identifier + '.log');
					if (exists) {
						//拷贝文件
						var ipaFold = path.resolve('IPA', identifier);
						copyFile.copyFile(ipaPath, ipaFold + '/a', function() {
							//修改名称
							var timestamp = Date.parse(new Date());
							var ipaName = identifier + shameName + '_' + timestamp + '.ipa';
							//log Path 

							fs.rename(ipaFold + '/' + shameName + '.ipa', ipaFold + '/' + ipaName, function(err) {
								if (err) {
									callback(false, task, logInfo, logPath);
									return;
								};
								// file.removeFold(taskPath, function(){});
								callback(true, task, ipaFold + "/" + ipaName, logInfo, logPath);
							})
						});
					} else {
						// file.removeFold(taskPath, function() {});
						if (callback) {
							callback(false, task, '', logInfo, logPath);
						};

					};
				});
			};

			console.log('1.1: results: ', results);
		});



}


exports.build = startBuild;