//
//  DataCenter.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

#define DEFAULT_ACCOUNT_NAME  @"guest"

@class RolePriviligeModel;

@interface DataCenter : NSObject

//设备device token
@property(nonatomic,strong) NSString* deviceToken;

//应用token
@property(nonatomic,strong) NSString* appToken;

//应用token 过期时间
@property(nonatomic,strong) NSDate* appTokeExpired;


//应用的plist
@property(nonatomic,strong) NSString* plist;

//登陆帐号
@property(nonatomic,strong) NSString* username;

//登陆token
@property(nonatomic,strong) NSString* accountToken;


//角色所拥有的权限列表
@property(nonatomic,strong) RolePriviligeModel* rolePriviligeModel;

//访客角色所拥有的权限列表
@property(nonatomic,strong) RolePriviligeModel* guestRolePriviligeModel;

//本地模块映射
@property(nonatomic,strong) NSMutableArray* moduleMaps;

//已下载模块
@property(nonatomic,strong) NSMutableArray* installMoudules;

//服务器模块
@property(nonatomic,strong) NSArray* serviceModules;


+(DataCenter*)defaultCenter;

@end
