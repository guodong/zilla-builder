//
//  CViewController.m
//  Main
//
//  Created by Fanty on 13-12-23.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "CViewController.h"
#import "MainUtils.h"


@interface CViewController ()

@end

@implementation CViewController

- (void)viewDidLoad{
    [super viewDidLoad];

    if([MainUtils isPad]){
        if(self.fullScreenForIPad){
            self.view.frame=CGRectMake(0.0f, 0.0f, 1024.0f, ([MainUtils isIOS7]?768.0f:748.0f));
        }
        else{
            CGRect rect=self.view.frame;
            rect.size=CGSizeMake(508.0f, 748.0f);
            self.view.frame=rect;
            
        }
    }
}

-(void)initNavigation{
    float topOffset=((![MainUtils isPad] && [MainUtils isIOS7])?20.0f:0.0f);
    
    
    navigationBar=[[UINavigationBar alloc] initWithFrame:CGRectMake(0.0f, topOffset, self.view.frame.size.width, 44.0f)];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0){
        navigationBar.barTintColor = [UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1];
        navigationBar.translucent = NO;
        UIColor * cc = [UIColor whiteColor];
        NSDictionary * dict = [NSDictionary dictionaryWithObject:cc forKey:UITextAttributeTextColor];
        navigationBar.titleTextAttributes = dict;
    }
    else{
        [navigationBar setTintColor: [UIColor colorWithRed:70/255.0 green:135/255.0 blue:199/255.0 alpha:1]];
    }

    navigationItem=[[UINavigationItem alloc] initWithTitle:self.title];
    
    UIImage* img=[UIImage imageNamed:@"Main.bundle/images/back.png"];
    UIButton* backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    float w=([MainUtils isIOS7]?img.size.width:img.size.width*2.0f);
    backButton.frame=CGRectMake(0.0f, 0.0f, w, img.size.height);
    [backButton setImage:img forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];

    navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:backButton];

    [navigationBar pushNavigationItem:navigationItem animated:NO];
    [self.view addSubview:navigationBar];

    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark method

-(void)backClick{
    if([self.navigationController.viewControllers count]<2){
        if([MainUtils isPad] && [self.delegate respondsToSelector:@selector(parentBackClick:)]){
            [self.delegate parentBackClick:self];
            return;
        }
    }
    if(self.navigationController==nil)
        [self dismissModalViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)createRightNavigationItem:(NSString*)title target:(id)target action:(SEL)action{
    
    UIButton* backButton=[UIButton buttonWithType:UIButtonTypeCustom];

    backButton.frame=CGRectMake(0.0f, 0.0f, 60.0f, 44.0f);
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backButton setTitle:title forState:UIControlStateNormal];
    [backButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:backButton];

}

@end
