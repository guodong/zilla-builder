//
//  MainModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-5.
//
//

#import <chameleon-ios-sdk/CModule.h>

@class Zilla;

@interface MainModule : CModule{
    Zilla* zillaSDK;
}

@end
