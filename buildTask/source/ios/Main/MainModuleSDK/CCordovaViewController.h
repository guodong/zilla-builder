//
//  CCordova.h
//  Main
//
//  Created by Fanty on 13-12-20.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Cordova/CDVViewController.h>

typedef void (^CCordovaViewControllerFinishBlock)(BOOL success);


@interface CCordovaViewController : CDVViewController
@property(nonatomic,assign) BOOL fullScreenForIPad;
@property(nonatomic,strong) NSDictionary* params;
@property(nonatomic,copy) CCordovaViewControllerFinishBlock finishBlock;

//读取html5  路径
-(void)loadFilePageWithFileUrl:(NSURL*)url;
-(void)startLoadNow;
@end
