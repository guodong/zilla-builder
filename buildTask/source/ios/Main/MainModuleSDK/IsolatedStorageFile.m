//
//  IsolatedStorageFile.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "IsolatedStorageFile.h"

@implementation IsolatedStorageFile


+(BOOL)copyFolderAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)aError{
    
    NSFileManager* fileManager=[NSFileManager defaultManager];
    NSError *error = nil;
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:srcPath error:&error];
    for (NSString *subfile in contents) {
        NSString *fullSrc = [srcPath stringByAppendingPathComponent:subfile];
        NSString *fullDst = [dstPath stringByAppendingPathComponent:subfile];
        
        BOOL srcItem_isDirectory = NO;
        [fileManager fileExistsAtPath:fullSrc isDirectory:&srcItem_isDirectory];
        
        
        if (srcItem_isDirectory) {//目录
            
            [fileManager createDirectoryAtPath:fullDst withIntermediateDirectories:YES attributes:nil error:&error];
            
            [self copyFolderAtPath:fullSrc toPath:fullDst error:&error];
        } else{//文件
            [fileManager removeItemAtPath:fullDst error:nil];
            [fileManager copyItemAtPath:fullSrc toPath:fullDst error:&error];
            
        }
    }
    return error == nil;
}

+(NSString*)cordovaResourceDirectory{
    NSURL* url= [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Main.bundle/www" isDirectory:YES];
    
    return [url path];
}

+(NSString*)cordovaRootDirectory{
    NSString* root=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    root=[root stringByAppendingPathComponent:@"www"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    return root;
}



@end
