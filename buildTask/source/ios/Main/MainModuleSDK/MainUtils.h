//
//  MainUtils.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

@interface MainUtils : NSObject

//是否为ipad
+(BOOL)isPad;

//是否iphone5
+(BOOL)isIphone5;

//是否ios7
+(BOOL)isIOS7;

@end
