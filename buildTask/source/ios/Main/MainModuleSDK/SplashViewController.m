//
//  SplashViewController.m
//  Main
//
//  Created by Fanty on 13-12-18.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "SplashViewController.h"

#import "MainUtils.h"
#import "CCordovaViewController.h"
#import <chameleon-ios-sdk/CApplication.h>


@interface SplashViewController ()
-(void)mainModule_LoadFinish;

@property(nonatomic,strong) CCordovaViewController* mainContentViewController;

@end

@implementation SplashViewController

-(id)init{
    self=[super init];
    if(self){
        self.fullScreenForIPad=YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainModule_LoadFinish) name:@"MainModule_LoadFinish" object:nil];
    }
    
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
            
    NSString* default_image=nil;
    if([MainUtils isPad])
        default_image=@"Default-Landscape~ipad.png";
    else
        default_image=([MainUtils isIphone5]?@"Default-568h@2x.png":@"Default.png");
    
    default_image=[[NSBundle mainBundle] pathForResource:default_image ofType:nil];
    
    UIImageView* imgView=[[UIImageView alloc] initWithFrame:self.view.bounds];
    if([MainUtils isPad])
        imgView.contentMode=UIViewContentModeScaleToFill;
    else
        imgView.contentMode=UIViewContentModeTop;
    imgView.image=[UIImage imageWithContentsOfFile:default_image];
    [self.view addSubview:imgView];
    
    UIActivityIndicatorView* loadingView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingView.frame=CGRectMake(0.0f, 0.0f, 64.0f, 64.0f);
    loadingView.center=self.view.center;
    [self.view addSubview:loadingView];
    [loadingView startAnimating];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


-(void)mainModule_LoadFinish{
    CCordovaViewController* controller=(CCordovaViewController*)[[CApplication sharedApplication] startMainViewControllerWithIdentifier:@"Module" params:nil];
    if([controller isKindOfClass:[CCordovaViewController class]]){
        self.mainContentViewController=controller;
        __block SplashViewController* splashViewController=self;
        [controller startLoadNow];
        [controller setFinishBlock:^(BOOL success){
            [splashViewController.navigationController pushViewController:splashViewController.mainContentViewController animated:YES];
            splashViewController.mainContentViewController=nil;
        }];
        
    }
}

-(void)cordovaWebViewLoadFinish:(CCordovaViewController*)cordovaViewController success:(BOOL)success{
    self.mainContentViewController=nil;
}




@end
