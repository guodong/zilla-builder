
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Cordova
#define COCOAPODS_POD_AVAILABLE_Cordova
#define COCOAPODS_VERSION_MAJOR_Cordova 3
#define COCOAPODS_VERSION_MINOR_Cordova 1
#define COCOAPODS_VERSION_PATCH_Cordova 0

// Cordova/Base
#define COCOAPODS_POD_AVAILABLE_Cordova_Base
#define COCOAPODS_VERSION_MAJOR_Cordova_Base 3
#define COCOAPODS_VERSION_MINOR_Cordova_Base 1
#define COCOAPODS_VERSION_PATCH_Cordova_Base 0

// SSZipArchive
#define COCOAPODS_POD_AVAILABLE_SSZipArchive
#define COCOAPODS_VERSION_MAJOR_SSZipArchive 0
#define COCOAPODS_VERSION_MINOR_SSZipArchive 3
#define COCOAPODS_VERSION_PATCH_SSZipArchive 1

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 0
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 9
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

