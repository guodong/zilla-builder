new FastClick(document.body);
//封装cordova的执行方法，加上回调函数
var cordovaExec = function(plugin, action, parameters, callback) {
	//alert("进入cordovaExec方法");
	cordova.exec(function(data) {
		//alert("进入cordovaExec方法 success");
		if (callback !== undefined) {
			callback();
		}
	}, function(err) {
		//alert("进入cordovaExec方法 err ");
	}, plugin, action, parameters === null || parameters === undefined ? [] : parameters);
};
var backToMain = function() {
	$(".back_btn").trigger("click");
};
//首页接受到信息，刷新页面
var receiveMessage = function(identifier, count, display) {
	console.log("AAA进入index revceiveMessage count = " + count + identifier + display);
	var $moduleTips = $(".module_Tips[moduletype='main'][identifier='" + identifier + "']");
	console.log("size=" + $moduleTips.size());
	if (display && count > 0) {
		$moduleTips.html(count);
		$moduleTips.show();
	} else {
		$moduleTips.hide();
	}
	if (myScroll) {
		myScroll.refresh();
		//myScroll.scrollTo(0, 1, 200, true);
	}

};

//自动更新查新界面
var refreshMainPage = function(identifier, type, moduleMessage) {
	console.log("进入refreshMainPage" + type + "..." + identifier);
	if (($('.homeclick').hasClass('active')) && isOver === 0) {
		//主页面
		console.log("进入main页面");
		/*addModule(identifier, "main", moduleMessage);
		$("li[identifier='" + identifier + "']").css('opacity', '0.5');*/
		isOver = isOver + 1;
		console.log("刷新。。。。");
		loadModuleList("CubeModuleList", "mainList", "main", function() {
			//gridLayout();
			if (myScroll) {
				myScroll.refresh();
			}
			isOver = isOver - 1;
		});
	}

};

$(".homeclick").bind("click", function() {
	if (!$(this).hasClass("active")) {
		$(".tab-item").removeClass("active");
		$(this).addClass("active");
			$(".managerclick").html('<img src="img/manager_gray.png" style="width:24px; height:24px;">').append('<div style="font-size:13px;color:#666666;">管理</div>');
		$(".homeclick").html('<img src="img/home_orange.png" style="width:24px; height:24px;">').append('<div style="font-size:13px;color:#8d8d8d;">主页</div>');

		//$('#top_left_btn').removeClass('back_bt_class');
		//gridLayout();
		$('.buttomContent').css('display', 'none');
		$("#wrapper").css("top", "44px");

		$('#title').html("变色龙");
		//$('#manager_btn').show();
		//$('#top_left_btn').addClass("left_btn");
		//$('#top_left_btn').removeClass("back_btn");

		//cordovaExec("CubeModuleOperator", "index");
		loadModuleList("CubeModuleList", "mainList", "main", function() {
			gridLayout();
			if (myScroll) {
				myScroll.refresh();
				$('#top_left_btn').removeClass("disabled");
			}

		});
	}

});
$(".managerclick").bind("click", function() {
	$(".tab-item").removeClass("active");
	$(this).addClass("active");
			$(".homeclick").html('<img src="img/home_gray.png" style="width:24px; height:24px;">').append('<div style="font-size:13px;color:#666666;">主页</div>');
	$(".managerclick").html('<img src="img/manager_orange.png" style="width:24px; height:24px;">').append('<div style="font-size:13px;color:#8d8d8d;">管理</div>');
	cordovaExec("CubeModuleOperator", "sync", [], function() {
		//alert("管理同步成功");


		//$('#top_left_btn').addClass("back_btn");
		//$('#top_left_btn').removeClass("left_btn");
		//$('#top_left_btn').addClass('back_bt_class');

		//$('#manager_btn').hide();
		//完成后设置listview
		$('.buttomContent').css('display', 'block');
		$("#wrapper").css("top", "94px");
		$('#title').html("模块管理");

		console.log("listLayout");
		loadModuleList("CubeModuleList", "uninstallList", "uninstall", function() {
			listLayout();
			console.log("loadModuleList 数据完成");
			$('.module_div ul li .curd_btn').css('display', 'inline');
			isOver = 0;
			var type = "uninstall";
			console.log("44");
			activeModuleManageBarItem(type);

			//cordovaExec("CubeModuleOperator", "manager");
			console.log("5");
			if (myScroll) {
				myScroll.refresh();
			}
			console.log("6");

		});
		console.log("同步成功");
		//myScroll.refresh();
	});


});
$(".settingclick").bind("click", function() {
	// $(".tab-item").removeClass("active");
	// $(this).addClass("active");
	cordovaExec("CubeModuleOperator", "setting");
});

//进度获取
var updateProgress = function(identifier, count) {
	console.log(identifier + '进入updateProgress');
	if (count == -1) {
		$(".module_div ul li .module_li_img .progress[identifier='" + identifier + "']").css('display', 'block');
	} else if (count >= 0 && count <= 100) {

		console.log("count>=0 && count <=100 " + count);

		$(".module_div ul li .module_li_img .progress[identifier='" + identifier + "']").css('display', 'block');

		$(".module_div ul li .module_li_img .progress[identifier='" + identifier + "'] .bar").css('width', count + "%");

		// var $crud_btn_2 = $(".module_li .curd_btn[identifier='" + identifier + "']");
		// $crud_btn_2.attr("disabled", "disabled");
		// var btn_title_2 = $crud_btn_2.html();
		// if (btn_title_2 == "安装") {
		// 	$crud_btn_2.html("正在安装");
		// } else if (btn_title_2 == "删除") {
		// 	$crud_btn_2.html("正在删除");
		// } else if (btn_title_2 == "更新") {
		// 	$crud_btn_2.html("正在更新");
		// }



	} else if (count == 101) {
		$(".module_div ul li .module_li_img .progress[identifier='" + identifier + "']").css('display', 'none');
		// var $crud_btn = $(".module_li .curd_btn[identifier='" + identifier + "']");
		// var btn_title = $crud_btn.html();
		// if (btn_title == "正在安装") {
		// 	$crud_btn.html("安装");
		// } else if (btn_title == "正在删除") {
		// 	$crud_btn.html("删除");
		// } else if (btn_title == "正在更新") {
		// 	console.log("title正在更新10101010101");
		// 	$crud_btn.html("更新");
		// }
		// $crud_btn.removeAttr("disabled");

	}

};

//模块增删，刷新模块列表(uninstall，install，upgrade)
var refreshModule = function(identifier, type, moduleMessage) {
	if ($('.managerclick').hasClass('active')) {
		console.log("进入refreshModule " + type + "..." + identifier);

		if (type === "uninstall") {
			console.log("进入uninstall");
			//已安装减一个
			console.log("已安装页面减一个");
			$(".module_li[moduletype='install'][identifier='" + identifier + "']").remove();
			//未安装加一个
			//addModule(identifier, "uninstall", moduleMessage);
			console.log("未安装的加一个成功");
			//更新有则减
			$(".module_li[moduletype='upgrade'][identifier='" + identifier + "']").remove();
		} else if (type === "install") {
			console.log("进入install");
			//未安装减一个
			$(".module_li[moduletype='uninstall'][identifier='" + identifier + "']").remove();
			$(".module_li[moduletype='upgrade'][identifier='" + identifier + "']").remove();
			//已安装加一个
			//addModule(identifier, "install", moduleMessage);
			//更新不变
		} else if (type === "upgrade") {
			//已安装替换
			//addModule(identifier, "install", moduleMessage);
			//更新减一个
			$(".module_li[moduletype='upgrade'][identifier='" + identifier + "']").remove();
			//未安装不变
		} else if (type === "main") {
			//addModule(identifier, "main", moduleMessage);
		}


	} else {
		//主页面
		console.log("主界面、、");
		//$("li[identifier='" + identifier + "']").css('opacity', '1');
		//判断模块 hidden
		var isHidden = $("li[identifier='" + identifier + "']").attr("hidden");
		console.log("是否显示？？？？" + isHidden);
		if (isHidden == "true") {
			$("li[identifier='" + identifier + "']").remove();
		}
	}
	checkModules();


};
var addModule = function(identifier, type, moduleMessage) {
	var mm = moduleMessage;
	if (!(moduleMessage instanceof Object)) {
		mm = $.parseJSON(moduleMessage);
	}
	//如果该模块不存在，则生成
	if ($(".scrollContent_li[modules_title='" + mm.category + "']").size() < 1) {
		var tag = $(".scrollContent_li").size();
		//获取模板名
		var moduleContentTemplate = $("#t2").html();

		var moduleContentHtml = _.template(moduleContentTemplate, {
			'muduleTitle': mm.category,
			"tag": tag
		});
		$(".mainContent").find(".scrollContent").append(moduleContentHtml);
	}

	//如果存在，先删除，再添加
	if ($("li[identifier='" + identifier + "']").size() > 0) {
		$("li[identifier='" + identifier + "']").remove();
	}

	var moduleItemTemplate = $("#module_div_ul").html();

	mm.releaseNote = subStrByCnLen(mm.releaseNote + "", 21);
	var t = type;
	if (t === "upgrade") {
		t = "upgradable";
	}
	var moduleItemHtml = _.template(moduleItemTemplate, {
		'icon': mm.icon,
		'name': mm.name,
		'moduleType': t,
		'identifier': mm.identifier,
		'version': mm.version,
		'releasenote': mm.releaseNote,
		'btn_title': changeBtnTitle(type),
		"updatable": mm.updatable,
		"local": mm.local,
		"msgCount": mm.msgCount,
		"hidden": mm.hidden
	});
	$(".scrollContent_li[modules_title='" + mm.category + "']").children('div').children('ul').append(moduleItemHtml);
	if (myScroll) {
		myScroll.refresh();
	}


};
//检查模块信息的完整性，如果没有模块，则隐藏
var checkModules = function() {
	console.log('AAAAAA----检查模块信息的完整性，如果没有模块，则隐藏');
	$.each($(".scrollContent_li"), function(index, data) {

		if ($(this).children('.module_div').children('.module_div_ul').children('.module_li').size() < 1) {
			$(this).remove();

		} else {
			var show_module_lis = $(this).children('.module_div').children('.module_div_ul').children('.module_li');
			$.each($(show_module_lis), function(i, show_module_li) {
				if ($(show_module_li).css('display') == "none") {
					console.log("有隐藏的对象");
					$(show_module_li).parent('.module_div_ul').parent('.module_div').parent('.scrollContent_li').remove();

				}
			});

		}

	});

};

var changeBtnTitle = function(type) {
	//alert("进入changeBtnTitle");
	switch (type) {
		case "install":
			return "删除";
			break;
		case "uninstall":
			return "安装";
			break;
		case "upgradable":
			return "更新";
			break;
		case "upgrade":
			return "更新";
			break;
		case "main":
			return "删除";
			break;

	}
	//alert("完成changeBtnTitle");
};
/*$(document).ready(function() {*/
// 选中模块操作菜单，传入需要激活的按钮名称（uninstall，install，upgrade）
var activeModuleManageBarItem = function(type) {
	// 移除所有选中
	$(".buttomContent .buttom_btn_group .btn").removeClass("active");
	// 选中当前点击
	$(".buttomContent .buttom_btn_group .btn[data='" + type + "']").addClass("active");
};

//点击模块的时候触发事件
var module_all_click = function() {
	$("li[identifier]").bind('click', function() {
		var type = $(this).attr("moduleType");
		var identifier = $(this).attr("identifier");
		console.log("AAAAAmodule_all_click----" + type + " == " + identifier);
		cordovaExec("CubeModuleOperator", "showModule", [identifier, type]);
	});
};

//点击curd按钮事件
var curd_btn_click = function() {
	console.log("操作按钮点击");
	$(".module_li .curd_btn").bind('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var that = this;
		$(this).attr("disabled", "disabled");
		var btn_title = $(this).html();


		var showMessage = "确定" + btn_title + "该模块";
		/*	navigator.notification.confirm(
			showMessage, // message

			function(buttonIndex) {
			    alert("2");
				if (buttonIndex === 1) {
					//alert("buttonIndex " + buttonIndex);
					$(that).removeAttr("disabled");
					return;
				} else if (buttonIndex === 2) {
					//	alert("buttonIndex " + buttonIndex);
					if (btn_title == "安装") {
						$(that).html("正在安装");
					} else if (btn_title == "删除") {
						$(that).html("正在删除");
					} else if (btn_title == "更新") {
						$(that).html("正在更新");
					}

					var type = $(that).attr("moduleType");
					console.log("202 type=" + type);
					var identifier = $(that).attr("identifier");

					var action = "";
					//alert("action "+action);
					if (type == "install") {
						action = "uninstall";
					} else if (type == "uninstall") {
						action = "install";
					} else if (type == "upgrade") {
						action = "upgrade";
					}
					alert('good');
					alert(action);
					//开始安装、删除
					cordovaExec("CubeModuleOperator", action, [identifier], function() {
						console.log("操作Callback");
						checkModules();
						$(that).removeAttr("disabled");
						console.log("更新完成更改title " + action);
						$(that).html(btn_title);
					});


				}
			}, // callback to invoke with index of button pressed
			'提示信息', // title
			'取消,确定' // buttonLabels
		);
*/


		if (btn_title == "安装") {
			$(this).html("正在安装");
		} else if (btn_title == "删除") {
			$(this).html("正在删除");
		} else if (btn_title == "更新") {
			$(this).html("正在更新");
		}



		var type = $(this).attr("moduleType");
		console.log("202 type=" + type);
		var identifier = $(this).attr("identifier");

		var action = "";
		if (type == "install") {
			action = "uninstall";
		} else if (type == "uninstall") {
			action = "install";
		} else if (type == "upgradable") {
			action = "upgrade";
		}
		//开始安装、删除

		cordovaExec("CubeModuleOperator", action, [identifier], function() {
			console.log("操作Callback");
			checkModules();
			$(this).removeAttr("disabled");
			console.log("更新完成更改title");
			$(this).html(btn_title);
		});



	});
};


var changeLayout = function(oldfilename, newfilename, type) {
	//replacejscssfile("css/gridview.css", "css/listview.css", "css");
	replacejscssfile(oldfilename, newfilename, type);
}


// 初始化界面

var initial = function(type, data) {
	console.log("AAAAAAAA initial=" + type);
	//alert("initial start");
	//alert("进入initial");
	console.log(data);
	var i = 0;
	_.each(data, function(value, key) {
		console.log("--------------key= " + key);
		//alert("一个分类");
		console.log(value);
		$("#myul").append(_.template($("#t2").html(), {
			'muduleTitle': key,
			'tag': i
		}));
		_.each((value), function(value, key) {
			// if (!(value instanceof Object)) {
			// 	console.log("value不是对象,就转他成对象");
			// 	value = $.parseJSON(value);
			// }
			console.log("value name  " + value.name + " ------------------------------------------------------------------------------");
			//更新的图标，如果在未安装里面，不应该出现
			if (type === 'uninstall') {
				value.updatable = false;
			}
//               var t = type;
//               if(t==="upgrade"){
//               alert(t);
//               t = "upgradable";
//               }

			value.name = subStrByCnLen(value.name + "", 9);
			if ($(this).hasClass('back_bt_class')) {
				value.name = subStrByCnLen(value.name + "", 5);
			}
               
			value.releaseNote = subStrByCnLen(value.releaseNote + "", 13);
               
               
               //value.icon = "http://images.apple.com/cn/hotnews/promos/images/promo_ipad_air.jpg";
			$('.module_div_ul[num="' + i + '"]').append(
				_.template($("#module_div_ul").html(), {
					'icon': value.icon,
					'name': value.name,
					'moduleType': type,
					'identifier': value.identifier,
					//'version': value.version,
					'releasenote': value.releaseNote,
					'btn_title': changeBtnTitle(type),
					"updatable": value.updatable,
					"local": value.local,
					"msgCount": value.msgCount,
					"hidden": value.hidden

				}));
		});


		i = i + 1;
	});
	i = 0;
	console.log("完成initial方法");
	//alert("initial end");
};


// 加载列表，渲染成html
var isOver = 0;
var loadModuleList = function(plugin, action, type, callback) {
	//alert("进入loadMuduleList");
	//if (isOver === 0) {
	//	isOver = isOver + 1;
	var accountName = "";

	//$(".mainContent").html("");
	$(".mainContent").remove();

	var mainContent = $('<div class="mainContent"><ul id="myul" class="scrollContent nav nav-list"></ul>');
	$("#scroller").append(mainContent);
	$(".mainContent").css('padding-bottom', '20px');

	console.log('AAAAAA------------------ZHE');
	// cordova.exec(success,failed,plugin,action,[]);
	//alert("loadModuleList 开始获取底层数据");
	cordova.exec(function(data) {
		//alert("loadModuleList 获取底层数据成功");
		console.log("2");
		if (!(data instanceof Object)) {
			console.log("不是对象");
			//data = $.parseJSON(data);
		} else {
			console.log("对象");
		}
		data = $.parseJSON(data);
		console.log("3");
		initial(type, data);
		//绑定点击事件
		//$("li[identifier]").die('click');



		module_all_click();
		//curd_btn_click();

		if (myScroll) {
			myScroll.refresh();
		}

		checkModules();
		// if ($('#top_left_btn').hasClass('back_bt_class')) {
		// 	listLayout();
		// }
		//isOver = isOver - 1;
		//如果回调方法不为空，则执行该回调方法
		if (callback !== undefined) {
			callback();
		}
		console.log("完成loadMuduleList");
	}, function(err) {
		isOver = isOver - 1;
	}, plugin, action, []);

	//}



};
// 左边按键--设置、返回
$('#top_left_btn')
	.bind("click",
		function() {
			$('#top_left_btn').addClass("disabled");
			isOver = 0;
			if ($(this).hasClass('back_bt_class')) {
				// 返回按键

				$('#top_left_btn').removeClass('back_bt_class');
				gridLayout();
				////alert("shanchu le back_bt_class");
				$('.buttomContent').css('display', 'none');
				$("#wrapper").css("top", "44px");

				$('#title').html("变色龙");
				$('#manager_btn').show();
				$('#top_left_btn').addClass("left_btn");
				$('#top_left_btn').removeClass("back_btn");

				//cordovaExec("CubeModuleOperator", "index");
				loadModuleList("CubeModuleList", "mainList", "main", function() {
					if (myScroll) {
						myScroll.refresh();
						$('#top_left_btn').removeClass("disabled");
					}

				});


			} else {

				// 设置按键
				$('#top_left_btn').removeClass("disabled");
				cordovaExec("CubeModuleOperator", "setting");
			}


		});


// 检测屏幕是否伸缩
var LastHeight = window.screen.availHeight;
$(window).resize(function() {
	var browser = navigator.appName;
	if (browser !== "Microsoft Internet Explorer") {
		var availHeight = $(window).height();
		console.log("LastHeight " + LastHeight);
		console.log("availHeight " + availHeight);
		if (Math.abs(LastHeight - availHeight) > 100) {
			if ((LastHeight - availHeight) > 0) {
				//键盘弹出
				console.log("键盘弹出了");

				$('.buttomContent').hide();
			} else {
				console.log("键盘隐藏了。。。");
				//键盘隐藏
				if ($('#top_left_btn').hasClass('back_bt_class')) {
					$('.buttomContent').show();
				}
			}
		}
		LastHeight = availHeight;
		if (myScroll) {
			myScroll.refresh();
		}
	}


});
$("#wrapper").bind("touchend", function() {
	if (myScroll) {
		myScroll.refresh();
	}

});
/*$(".del_content").click(function() {
	console.log("点击了");
	$("#searchInput").val("");
	$(".del_content").hide();

	var scrollContent_lis = $('#myul').children('li');
	$.each(scrollContent_lis, function(index, li) {
		var module_lis = $(li).children('div').children('ul').children('li');
		$.each(module_lis, function(index, module_li) {
			$(module_li).show();
		});

		scrollContent_lis.show();
		// end
	});



});*/
// 搜索框事件
/*$("#searchInput").bind("input propertychange", function() {
	var me = $(this);
	var keyword = me.val();
	if (keyword == "" || keyword == undefined || keyword == null) {
		$(".del_content").hide();
	} else {
		$(".del_content").css("display", "inline");
	}
	console.log("keyword=" + keyword);
	var scrollContent_lis = $('#myul').children('li');
	$.each(scrollContent_lis, function(index, li) {
		var module_lis = $(li).children('div').children('ul').children('li');
		var i = 0;
		$.each(module_lis, function(index, module_li) {

			var titlename = $(module_li).children('div:nth-child(3)').children('div:nth-child(1)').html();
			if (keyword !== "") {
				console.log(keyword.length + "keyword 之前" + keyword);
				keyword = trim(keyword);
				console.log(keyword.length + "keyword 之后" + keyword);
				if (titlename.toLowerCase().indexOf(keyword.toLowerCase()) < 0) {
					$(module_li).hide();
				} else {
					$(module_li).show();
					i++;
				}
			} else {
				$(module_li).show();
			}
		});
		if (i > 0) {
			$(li).show();
		} else {
			$(li).hide();
		}

		i = 0;
		if (keyword == "") {
			scrollContent_lis.show();
		}
		// end
	});

	setTimeout(function() {
		if (myScroll) {
			myScroll.refresh();
		}
	}, 100);
});
*/
var listLayout = function() {

	console.log("listview");
	changeLayout("css/gridview.css", "css/listview.css", "css");
	/*	if ($('#top_left_btn').hasClass('back_bt_class')) {*/
	//管理界面
	//显示 curd_btn
	//$('.module_div ul li .curd_btn').css('display', 'inline');
	//隐藏 > 图标
	//$('.module_div ul li .icon-chevron-right').css('display', 'none');
	//$('.detail .module_li_titlename').css('font-size', '1.4em');

	//禁止本地模块curd_btn显示
	// $.each($('.module_div ul .module_li .curd_btn'), function(index, item) {
	// 	if ($(item).attr('local') === null || $(this).attr('local') === undefined || $(item).attr('local') == "") {
	// 		$(this).show();
	// 	} else {
	// 		$(this).hide();
	// 	}

	// });
	/*	} else {
		alert("not has back_bt_class");
		//查看界面
		//设置 > 图标显示
		$('.module_div ul li .icon-chevron-right').css('display', 'inline');
		//隐藏version显示
		$('.detail .module_li_version').css('display', 'none');
		//隐藏releasenote显示
		$('.detail .module_li_releasenote').css('display', 'none');
		//设置titlename位置
		$('.detail .module_li_titlename').css('top', '15px').css('font-size', '1.2em');


	}*/
	//切换active
	$('#listview_btn').addClass('active');
	$('#gridview_btn').removeClass('active');

	setTimeout(function() {
		if (myScroll) {
			myScroll.refresh();
			myScroll.scrollTo(0, 0, 200, false);
		}

	}, 100);
};

var gridLayout = function() {
	console.log("gridview");
	changeLayout("css/listview.css", "css/gridview.css", "css");


	$("li[identifier]").css("background", "#ffffff");
	//隐藏 > 标记图
	//$('.module_div ul li .icon-chevron-right').css('display', 'none');
	//隐藏curd_btn
	//$('.module_div ul li .curd_btn').css('display', 'none');
	//设置titlename位置
	$('.detail .module_li_titlename').css('font-size', '1em').css("top", "7px");
	//切换active
	$('#gridview_btn').addClass('active');
	$('#listview_btn').removeClass('active');

	setTimeout(function() {
		if (myScroll) {
			myScroll.refresh();
			//myScroll.scrollTo(0, 0, 200, false);
		}

	}, 100);
}

/*$('#listview_btn').bind('click', function() {
	if (!$('#listview_btn').hasClass("active")) {
		listLayout();
		setTimeout(function() {
			if (myScroll) {
				myScroll.refresh();
			}
		}, 100);
	}

});

$('#gridview_btn').bind('click', function() {
	if (!$('#gridview_btn').hasClass("active")) {
		gridLayout();
		setTimeout(function() {
			if (myScroll) {
				myScroll.refresh();
			}
		}, 100);
	}

});*/

// 管理按钮
$('#manager_btn')
	.click(function() {
		$('#manager_btn').addClass("disabled");
		console.log("点击");

		cordovaExec("CubeModuleOperator", "sync", [], function() {
			//alert("管理同步成功");

			// $('#manager_btn').removeClass("disabled");
			// $('#top_left_btn').addClass("back_btn");
			// $('#top_left_btn').removeClass("left_btn");
			// $('#top_left_btn').addClass('back_bt_class');

			// $('#manager_btn').hide();
			//完成后设置listview
			$('.buttomContent').css('display', 'block');
			$("#wrapper").css("top", "94px");
			$('#title').html("模块管理");

			console.log("listLayout");
			loadModuleList("CubeModuleList", "uninstallList", "uninstall", function() {
				//listLayout();
				console.log("loadModuleList 数据完成");
				$('.module_div ul li .curd_btn').css('display', 'inline');
				isOver = 0;
				var type = "uninstall";
				console.log("44");
				activeModuleManageBarItem(type);

				//cordovaExec("CubeModuleOperator", "manager");
				console.log("5");
				if (myScroll) {
					myScroll.refresh();
				}
				console.log("6");

			});
			console.log("同步成功");
			//myScroll.refresh();
		});
		//$('#manager_btn').removeClass("disabled");
	});


//处理底下按钮
$(".buttomContent .buttom_btn_group .btn").click(function() {
	var type = $(this).attr("data");
	console.log("butom type=" + type);
	if (!$(this).hasClass("active")) {
		activeModuleManageBarItem(type);
		var t = type;
		if (type == "upgrade") {
			type = "upgradable";
		}

		loadModuleList("CubeModuleList", type + "List", t, function() {
			//listLayout();
			if (myScroll) {
				myScroll.refresh();
			}

		});

	}

});


//---------------------------------------------------------------------------------------------


//应用初始化
var app = {
	initialize: function() {
		console.log("initialize");
		this.bindEvents();
	},
	bindEvents: function() {
		console.log("bindEvents");
		document.addEventListener('deviceready', this.onDeviceReady, false);
		/*var browser = navigator.appName;
		if (browser !== "Microsoft Internet Explorer") {
			new FastClick(document.body);
		}*/
	},
	onDeviceReady: function() {
		console.log("onDeviceReady");
		app.receivedEvent('deviceready');
	},
	receivedEvent: function(id) {
		console.log("receivedEvent");
		loadModuleList("CubeModuleList", "mainList", "main", function() {
			//alert("receivedEvent loadModuleList完成");
		});
		cordovaExec("CubeModuleOperator", "sync", [], function() {
			console.log("同步成功");
			loadModuleList("CubeModuleList", "mainList", "main", function() {
				if (myScroll) {
					myScroll.refresh();

				}
			});
		});
	}
};
app.initialize();