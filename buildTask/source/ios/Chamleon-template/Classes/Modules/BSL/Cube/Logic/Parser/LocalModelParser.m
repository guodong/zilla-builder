//
//  LocalModelParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import "LocalModelParser.h"
#import "CubesModel.h"

@implementation LocalModelParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSArray class]]){
        
        NSMutableArray* list=[[NSMutableArray alloc] initWithCapacity:2];
        NSArray* array=jsonMap;
        
        for(NSDictionary* dict in array){
            LocalModel* model=[[LocalModel alloc] init];
            model.identifier=[dict objectForKey:@"identifier"];
            model.icon=[dict objectForKey:@"icon"];
            model.action=[dict objectForKey:@"action"];
            [list addObject:model];
            
        }
        result=list;
    }
}

@end
