//
//  JSUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import <Foundation/Foundation.h>

@interface JSUtils : NSObject

+(NSString*)refreshModule:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage;

+(NSString*)refreshMainPage:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage;

+(NSString*)receiveMessage:(NSString*)identifier count:(int)count;

+(NSString*)updateProgress:(NSString*)identifier count:(int)count;

+(NSString*)logout;

@end
