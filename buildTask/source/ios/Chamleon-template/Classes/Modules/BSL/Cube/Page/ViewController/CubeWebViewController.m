//
//  CubeWebViewController.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-9.
//
//

#import "CubeWebViewController.h"
#import "IsolatedStorageFile(CubePage).h"

@interface CubeWebViewController ()
-(void)redirectToCubeMainPage;
@end

@implementation CubeWebViewController{
    UIButton* closeButton;
}

- (void)viewDidLoad{
    
    if([self.identifier length]>0){
        NSURL* url=[NSURL fileURLWithPath:[IsolatedStorageFile cubeModuleIdentifierMainPage:self.identifier]];
        [self loadFilePageWithFileUrl:url];
    }
    [super viewDidLoad];
    
    CGRect rect=self.view.frame;
    rect.size.height-=20.0f;
    self.view.frame=rect;
    
    rect=self.webView.frame;
    rect.size.height=self.view.bounds.size.height-20.0f;
    self.webView.frame=rect;


    closeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* img=[UIImage imageNamed:@"gohome.png"];
    [closeButton setImage:img forState:UIControlStateNormal];
    
    closeButton.frame=CGRectMake(0.0f, self.view.frame.size.height-img.size.height*2.0f, img.size.width*2.0f, img.size.height*2.0f);
    [closeButton addTarget:self action:@selector(redirectToCubeMainPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];

    
    self.webView.delegate=self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark webvlew delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL* url = [request URL];
    NSLog(@"请求页面: %@", url);
    if ([@"cube://exit" isEqualToString:[url absoluteString]]) {
        [self.navigationController popToViewController:self.rootViewController animated:YES];
        return [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    }
    
    if ([[url absoluteString] rangeOfString:@"cube-action"].location != NSNotFound) {
        
        NSRange range = [[url absoluteString] rangeOfString:@"cube-action"];
        NSString *tempaction = [[url absoluteString] substringFromIndex:range.location + range.length + 1];
        NSRange sharp = [tempaction rangeOfString:@"#"];
        NSString *action = tempaction;
        if(sharp.length != 0){
            action = [tempaction substringToIndex:sharp.location];
        }
        
        
        if ([@"pop" isEqualToString:action]) {
            if([self.navigationController.viewControllers count]>2){
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"POP_DISMISS_VIEW" object:nil];
            }
        } else if([@"push" isEqualToString:action]) {
            
            //remove ?cube-action
            url = [NSURL URLWithString:
                   [[url absoluteString] stringByReplacingOccurrencesOfString:@"?cube-action=push" withString:@""]];
            
            NSString *path = url.path;
            NSRange range = [path rangeOfString:@"www/"];
            if(range.location != NSNotFound){
//                path = [path substringFromIndex:range.location + range.length];
//                NSRange range2 = [path rangeOfString:@"/"];;
//                path = [path substringToIndex:range2.location];
                //dispatch_async(dispatch_get_main_queue(), ^{
//                [OperateLog recordOperateLogWithIdentifier:path];
                //});
            }
            
            NSURLRequest *newRequest = [NSURLRequest requestWithURL:url cachePolicy:[request cachePolicy] timeoutInterval:[request timeoutInterval]];
            
            CubeWebViewController* controller=[[CubeWebViewController alloc] init];
            controller.rootViewController=self.rootViewController;
            [controller loadFilePageWithFileUrl:[newRequest URL]];
            [self.navigationController pushViewController:controller animated:YES];
        }
        return NO;
    }

    NSString *timeSp = [NSString stringWithFormat:@"?date=%ld", (long)[[NSDate date]  timeIntervalSince1970]];
    
    url =  [url URLByAppendingPathComponent:timeSp];
    
    NSURLRequest *newRequest = [NSURLRequest requestWithURL:url cachePolicy:[request cachePolicy] timeoutInterval:[request timeoutInterval]];
    
    return [super webView:webView shouldStartLoadWithRequest:newRequest navigationType:navigationType];
}


#pragma mark method

-(void)redirectToCubeMainPage{
    [self.navigationController popToViewController:self.rootViewController animated:YES];
}

@end
