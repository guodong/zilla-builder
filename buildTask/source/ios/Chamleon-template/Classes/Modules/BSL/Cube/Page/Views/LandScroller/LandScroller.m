//
//  LandScroller.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "LandScroller.h"
#import "ImageLoadingDownloadedView.h"
#import "ApiConstant+Cube.h"
#import "DataCenter.h"
@implementation LandScroller

- (id)init{
    self = [super init];
    if (self) {
        // Initialization code
        self.userInteractionEnabled=YES;
        self.image=[UIImage imageNamed:@"introduce_detail_snapshotbg.png"];
        CGRect frame=self.frame;
        frame.size=self.image.size;
        self.frame=frame;
        
        scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 30.0f, frame.size.width, frame.size.height-50.0f)];
        scrollView.showsVerticalScrollIndicator=NO;
        [self addSubview:scrollView];
        
    }
    return self;
}
-(void)setImages:(NSArray*)array{
    float left=0.0f;
    NSString* sessionKey=[[DataCenter defaultCenter].dictonary objectForKey:@"bsl-sessionkey"];

    for(NSString* val in array){
        ImageLoadingDownloadedView* imgView=[[ImageLoadingDownloadedView alloc] initWithFrame:CGRectMake(left, 0.0f, scrollView.frame.size.width, scrollView.frame.size.height)];
        [imgView setUrl:[[[ApiConstant defaultConstant] attachmentByIdURL:val sessionKey:sessionKey] absoluteString]];
        [scrollView addSubview:imgView];
        imgView.contentMode=UIViewContentModeScaleAspectFit;
        left+=scrollView.frame.size.width;
    }
    scrollView.contentSize=CGSizeMake(left, scrollView.frame.size.height);
}

@end
