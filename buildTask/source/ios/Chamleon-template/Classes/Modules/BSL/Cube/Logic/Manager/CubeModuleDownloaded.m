//
//  CubeModuleDownloaded.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import "CubeModuleDownloaded.h"
#import "ASIHTTPRequest.h"
#import "IsolatedStorageFile(CubeLogic).h"
#import "ApiConstant+Cube.h"
#import "CubesModel.h"
#import "DataCenter.h"
#import "CubeModule.h"
#import <ChamleonSDK/CApplication.h>

@interface CubeModuleDownloaded()

-(void)eventprocess;
-(void)failed;
-(void) success;

-(void)callServerToDownload:(int)index;

@end

@implementation CubeModuleDownloaded


- (void)dealloc{
    NSLog(@"CubeModuleDownloaded dealloc");
}

-(void)cancel{
    [processTimer invalidate];
    processTimer=nil;
    [request setCompletionBlock:nil];
    [request setFailedBlock:nil];
    [request cancel];
    request=nil;
}


-(void)start{
    [self cancel];
    [self callServerToDownload:0];
}

-(void) failed{
    [self.callback failed];
}

-(void) success{
    [self.callback unzip];

}

-(void)eventprocess{
    [processTimer invalidate];
    processTimer=nil;
    [self.callback process];
}

-(void)headersReceived:(int)contentLength{
    self.cubeModel.downloadedTotalCount=contentLength;
}

-(void)bytesReceivedBlock:(unsigned long long)size{
    self.cubeModel.downloadedProcess+=size;
    
    if(processTimer!=nil){
        return ;
    }
    
    processTimer=[NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(eventprocess) userInfo:nil repeats:NO];

}
-(void)requestComplete:(NSString*)filePath index:(int)index{
    [processTimer invalidate];
    processTimer=nil;
    
    [request setCompletionBlock:nil];
    [request setFailedBlock:nil];
    request=nil;
    
    BOOL _success=NO;
    const char* __path =[filePath UTF8String];
    FILE* fp=fopen(__path, "rb");
    if(fp!=nil){
        fseek(fp, 0, SEEK_END);
        long count=ftell(fp);
        fclose(fp);
        
        if(count==self.cubeModel.downloadedTotalCount){
            _success=YES;
            //success
        }
    }
    if(!_success){
        if(index<3){
            [self callServerToDownload:(index+1)];
        }
        else{
            [self failed];
        }
    }
    else{
        [self success];
    }

}

-(void)requestFailed:(int)index{
    [processTimer invalidate];
    processTimer=nil;
    
    [request setCompletionBlock:nil];
    [request setFailedBlock:nil];
    request=nil;

    if(index<3){
        [self callServerToDownload:(index+1)];
    }
    else{
        [self failed];
    }

}

-(void)callServerToDownload:(int)index{
    [self cancel];
    
    NSString* filePath=[IsolatedStorageFile cubeModelTempZip:self.cubeModel.identifier];
    

    CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];

    request=[ASIHTTPRequest requestWithURL:[[ApiConstant defaultConstant] attachmentByIdURL:self.cubeModel.bundle sessionKey:module.sessionKey]];
    request.timeOutSeconds=20.0f;
    request.persistentConnectionTimeoutSeconds=20.0f;
    [request setDownloadDestinationPath:filePath];
    
    __block CubeModuleDownloaded* objSelf=self;
    [request setHeadersReceivedBlock:^(NSDictionary* headers){
        [objSelf headersReceived:[[headers objectForKey:@"Content-Length"] intValue]];
    }];
    
    [request setCompletionBlock:^{
        [objSelf requestComplete:filePath index:index];
    }];
    
    [request setFailedBlock:^{
        [objSelf requestFailed:index];
    }];
    
    [request setBytesReceivedBlock:^(unsigned long long size, unsigned long long total) {
        
        [objSelf bytesReceivedBlock:size];
    }];
    
    
    [request startAsynchronous];

    
    
}


@end
