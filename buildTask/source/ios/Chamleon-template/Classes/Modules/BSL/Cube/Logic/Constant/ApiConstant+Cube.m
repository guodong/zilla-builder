//
//  ApiConstant+Login.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "ApiConstant+Cube.h"

@implementation ApiConstant(Cube)

-(NSURL*)moduleListURL:(NSString*)sessionKey username:(NSString*)username{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:18860/mam/api/mam/clients/ios/%@/%@/?username=%@&sessionKey=%@&timeStamp=%f",self.serverURLHost,[[NSBundle mainBundle] bundleIdentifier],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],username,sessionKey,[NSDate timeIntervalSinceReferenceDate]]];
    
}

-(NSURL*)attachmentByIdURL:(NSString*)aId sessionKey:(NSString*)sessionKey{
        
    NSString* build=[NSString stringWithFormat:@"http://%@:18860/mam/api/mam/clients/files/%@?appKey=%@",self.serverURLHost,aId,self.appKey];
    if([sessionKey length]>0){
        build=[build stringByAppendingFormat:@"&sessionKey=%@",sessionKey];
    }
    return [NSURL URLWithString:build];
}

-(NSURL*)snapshotURL:(NSString*)identifier version:(NSString*)version{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:18860/mam/api/mam/clients/widget/%@/%@/snapshot?appKey=%@",self.serverURLHost,identifier,version,self.appKey]];
}

@end

