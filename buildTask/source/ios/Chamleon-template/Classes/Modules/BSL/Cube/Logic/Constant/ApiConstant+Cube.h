//
//  ApiConstant+Login.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "ApiConstant.h"
@interface ApiConstant(Cube)


-(NSURL*)moduleListURL:(NSString*)sessionKey username:(NSString*)username;


-(NSURL*)attachmentByIdURL:(NSString*)aId sessionKey:(NSString*)sessionKey;

-(NSURL*)snapshotURL:(NSString*)identifier version:(NSString*)version;

@end
