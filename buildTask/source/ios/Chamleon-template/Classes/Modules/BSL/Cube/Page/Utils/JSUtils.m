//
//  JSUtils.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import "JSUtils.h"

@implementation JSUtils

+(NSString*)refreshModule:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage{
    return [NSString stringWithFormat:@"refreshModule('%@','%@','%@');",identifier,type,moduleMessage ];
}

+(NSString*)refreshMainPage:(NSString*)identifier type:(NSString*)type moduleMessage:(NSString*)moduleMessage{
    return [NSString stringWithFormat:@"refreshMainPage('%@','%@','%@');",identifier,type,moduleMessage ];

}

+(NSString*)receiveMessage:(NSString*)identifier count:(int)count{
    return [NSString stringWithFormat:@"receiveMessage('%@',%d,true);",identifier,count];
}

+(NSString*)updateProgress:(NSString*)identifier count:(int)count{
    return [NSString stringWithFormat:@"updateProgress('%@',%d);",identifier,count];

}

+(NSString*)logout{
    return @"clearPsw();";
}


@end
