//
//  MainModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import <ChamleonSDK/CModule.h>
#import "CubeModuleManager.h"
#import "AutoDownloadCheckingOperator.h"

@interface CubeModule : CModule{
    CubeModuleManager* cubeModuleManager;
    AutoDownloadCheckingOperator* autoDownloadCheckingOperator;

}

@property(nonatomic,strong) NSString* username;
@property(nonatomic,strong) NSString* sessionKey;

@property(nonatomic,readonly) CubeModuleManager* cubeModuleManager;
@property(nonatomic,readonly) AutoDownloadCheckingOperator* autoDownloadCheckingOperator;


@end
