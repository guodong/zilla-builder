//
//  DataCenter+Cube.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "DataCenter+Cube.h"
#import "CubesModel.h"
#import <objc/runtime.h>


@implementation DataCenter(Cube)


-(void)setLocalModules:(NSMutableArray *)value{
    [self setObject:value forKey:@"bsl-localmodules"];

}

-(NSMutableArray*)localModules{
    return (NSMutableArray*)([self.dictonary objectForKey:@"bsl-localmodules"]);

}

-(void)setInstallMoudules:(NSMutableArray *)value{
    [self setObject:value forKey:@"bsl-installmodules"];
}

-(NSMutableArray*)installMoudules{
    return (NSMutableArray*)([self.dictonary objectForKey:@"bsl-installmodules"]);
}


-(void)setServiceModules:(NSArray *)value{
    [self setObject:value forKey:@"bsl-servicemodules"];
}

-(NSArray*)serviceModules{
    return (NSArray*)([self.dictonary objectForKey:@"bsl-servicemodules"]);
}



-(NSDictionary*)mainListCategroyMap{
    NSArray* array=[self mainListModules];
    return [self moduleListToCategoryMap:array];
}


-(NSArray*)mainListModules{
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
    NSArray* installMoudules=self.installMoudules;
    for (CubeModel* model in installMoudules){
        //没有权限不允许加进去
        if (model.hasPrivileges){
            //模块正在下载时必须显示在主页面，但下载完该隐藏得隐藏
            if (model.hidden == NO ||
                model.status == CubeMoudleStatusInstalling ||
                model.status == CubeMoudleStatusUpdating)
                [array addObject:model];
        }
    }
    return array;
}

-(NSArray*)uninstallModules{
    NSMutableArray* result=[[NSMutableArray alloc] initWithCapacity:2];

    NSArray* installMoudules=self.installMoudules;
    NSArray* serviceModules=self.serviceModules;
    for (CubeModel* model in serviceModules){
        //已安装列表不包含，则为未安装模块
        if(model.hasPrivileges){
            BOOL hasExist=NO;
            for(CubeModel* _model in installMoudules){
                if([_model.identifier isEqualToString:model.identifier]){
                    hasExist=YES;
                    break;
                }
            }
            if(!hasExist)
                [result addObject:model];
        }
    }
    
    return result;
}

-(NSArray*)updatableModules{
    NSMutableArray* result=[[NSMutableArray alloc] initWithCapacity:2];
    
    NSArray* installMoudules=self.installMoudules;
    NSArray* serviceModules=self.serviceModules;
    for (CubeModel* model in installMoudules){
        //已安装列表不包含，则为未安装模块
        if(model.hasPrivileges){
            BOOL mustUpdate=NO;
            for(CubeModel* _model in serviceModules){
                if([_model.identifier isEqualToString:model.identifier]){
                    if(_model.build>model.build)
                        mustUpdate=YES;
                    break;
                }
            }
            if(mustUpdate)
                [result addObject:model];
        }
    }
    
    return result;
}


-(NSDictionary*) moduleListToCategoryMap:(NSArray*)modelList{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc] initWithCapacity:1];
    for (CubeModel* model in modelList){
        
        NSMutableArray* list=[dict objectForKey:model.category];
        if(list==nil){
            list=[[NSMutableArray alloc] initWithCapacity:3];
            [dict setObject:list forKey:model.category];
        }
        [list addObject:model];
    }
    return dict;
}

-(NSDictionary*)uninstallCategroyMap{
    NSArray* array=[self uninstallModules];
    return [self moduleListToCategoryMap:array];
}

-(NSDictionary*)installCategroyMap{
    return [self moduleListToCategoryMap:self.installMoudules];
}

-(NSDictionary*)updatableCategroyMap{
    NSArray* array=[self updatableModules];
    return [self moduleListToCategoryMap:array];

}


-(CubeModel*)finidInstallCubeModule:(NSString*)identifier{
    for (CubeModel* model in self.installMoudules){
        if([model.identifier isEqualToString:identifier]){
            return model;
        }
    }
    
    return nil;
}

-(CubeModel*)finidServiceCubeModule:(NSString*)identifier{
    for (CubeModel* model in self.serviceModules){
        if([model.identifier isEqualToString:identifier]){
            return model;
        }
    }
    
    return nil;

}

@end
