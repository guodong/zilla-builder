//
//  CubeWebViewController.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-9.
//
//

#import <ChamleonSDK/CCordovaViewController.h>

@interface CubeWebViewController : CCordovaViewController

@property(nonatomic,strong) NSString* url;
@property(nonatomic,strong) NSString* identifier;

@property(nonatomic,weak) UIViewController* rootViewController;

@end
