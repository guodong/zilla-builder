//
//  CubeDataPathUtils.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import "IsolatedStorageFile(CubeLogic).h"
#import <ChamleonSDK/CApplication.h>
#import "CubesModel.h"
#import "DataCenter+Cube.h"
#import "ApiConstant+Cube.h"
#import "CubeModule.h"


@implementation IsolatedStorageFile(CubeLogic)

+(NSString*)localMoudlelistDirectory:(NSString*)account{
    NSString* root=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    root=[root stringByAppendingPathComponent:account];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"data"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"cube.json"];
    
    return root;
}

+(NSString*)loadAutoSaveFile:(NSString*)account{
    NSString* root=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    root=[root stringByAppendingPathComponent:account];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"data"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"autosave.json"];
    
    return root;
    
}

+(NSString*)localModelInfoFile{
    return [[NSBundle mainBundle] pathForResource:@"local_module_config" ofType:@"json"];
}

+(NSString*)cubeModelTempZip:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    

    return [root stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",identifier]];
}

+(NSString*)cubeModuleIdentifierRoot:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];

    root=[root stringByAppendingPathComponent:identifier];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }

    return root;
}

+(NSString*)landIconLocalFile:(NSString*)identifier{
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];

    return [root stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/icon.png",identifier]];
}

+(NSString*)outputIcon:(CubeModel *)cubeModel{
    if ([cubeModel.local length]>0){
        //本地模块，读取本地文件作为头像
        //return _icon;
        
        NSArray* localModules=[DataCenter defaultCenter].localModules;
        
        for (LocalModel* localModule in localModules){
            if ([localModule.identifier isEqualToString:cubeModel.identifier])
                return localModule.icon;
        }
        
        return @"";
    }
    else if (cubeModel.status == CubeMoudleStatusFinish || cubeModel.status==CubeMoudleStatusCanUpdate){
        //模块已安装，用文件夹中的头像
        //return _icon;
        
        NSString* image = [IsolatedStorageFile landIconLocalFile:cubeModel.identifier];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:image isDirectory:NO]){

            return image;
        }
        if([cubeModel.icon rangeOfString:@"http://"].length>0)
            return cubeModel.icon;
        else{
            
            CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
            
            return [[[ApiConstant defaultConstant] attachmentByIdURL:cubeModel.icon sessionKey:module.sessionKey] absoluteString];
        }
    }
    else{
        if([cubeModel.icon rangeOfString:@"http://"].length>0)
            return cubeModel.icon;
        else{
            CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"bsl.cube"];
            
            return [[[ApiConstant defaultConstant] attachmentByIdURL:cubeModel.icon sessionKey:module.sessionKey] absoluteString];

        }
    }
}


@end
