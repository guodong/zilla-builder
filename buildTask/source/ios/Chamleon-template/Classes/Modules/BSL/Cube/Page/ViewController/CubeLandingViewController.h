//
//  CubeLandingViewController.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-6.
//
//

#import <ChamleonSDK/CViewController.h>
@class CubeModel;
@class ImageDownloadedView;
@class LandScroller;
@class AsyncTask;
@interface CubeLandingViewController : CViewController{
    
    ImageDownloadedView* imageView;
    
    UILabel* titleView;
    UILabel* vertionView;
    UILabel* descView;
    UIButton* confirmButton;
    
    LandScroller* landScroller;
    
    UIProgressView* progressView;
    
    AsyncTask* task;
}
@property(nonatomic,assign) int type;
@property(nonatomic,strong) NSString* identifier;

@end
