//
//  CheckOperatorDelegate.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>
@class ASIHTTPRequest;
@protocol CheckOperatorDelegate <NSObject>

-(BOOL)checkResponseString:(ASIHTTPRequest*)request;
-(int)statusCode;
-(int)errorCode;
-(NSString*)errorMessage;
@end
