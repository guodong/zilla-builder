//
//  MainUtils.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "MainUtils.h"

@implementation MainUtils

+(BOOL)isPad{
    return ([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad);
}


+(BOOL)isIphone5{
    if([self isPad])return NO;
    return ( [UIScreen mainScreen].bounds.size.width>=1000.0f || [UIScreen mainScreen].bounds.size.width>1000.0f);
}


@end
