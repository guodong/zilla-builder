//
//  JsonParser.m
//
//
//  Created by fanty on 13-4-28.
//  Copyright (c) 2013年 . All rights reserved.
//

#import "JsonParser.h"
#import "JSONKit.h"

@implementation JsonParser

-(void)parse:(NSData*)data{
    result=nil;
    
    id jsonMap  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    [self onParser:jsonMap];
    
}

-(void)onParser:(id)jsonMap{
    
}

-(id)getResult{
    return result;
}

@end
