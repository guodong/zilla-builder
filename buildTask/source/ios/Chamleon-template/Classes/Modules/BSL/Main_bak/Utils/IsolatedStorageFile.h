//
//  IsolatedStorageFile.h
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import <Foundation/Foundation.h>

@interface IsolatedStorageFile : NSObject

+(BOOL)copyFolderAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)aError;


+(NSString*)cordovaResourceDirectory;

+(NSString*)cordovaRootDirectory;

@end
