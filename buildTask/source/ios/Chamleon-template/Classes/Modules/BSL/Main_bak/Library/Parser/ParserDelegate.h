//
//  ParserDelegate
//
//
//  Created by fanty on 13-5-29.
//  Copyright (c) 2013年 fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ParserDelegate <NSObject>

-(void)parse:(NSData*)data;
-(id)getResult;
@end
