//
//  AsyncTask.h
//  SVW_STAR
//
//  Created by fanty on 13-5-28.
//  Copyright (c) 2013年 fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserDelegate.h"
#import "CheckOperatorDelegate.h"

typedef void (^AsyncTaskBlock)();

@class ASIHTTPRequest;
@class ApiError;

@interface AsyncTask : NSObject
@property(nonatomic,strong) ASIHTTPRequest* request;
@property(nonatomic,strong) id<CheckOperatorDelegate> checkOperator;
@property(nonatomic,strong) id<ParserDelegate> parser;
@property (copy, nonatomic) AsyncTaskBlock finishBlock;

-(void)cancel;

-(void)start;

-(id)result;

-(int)statusCode;

-(NSString*)errorMessage;

-(NSData*)responseData;

@end


