//
//  BaseCheckOperator.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "BaseCheckOperator.h"
#import "ASIHTTPRequest.h"

@implementation BaseCheckOperator

-(BOOL)checkResponseString:(ASIHTTPRequest*)request{

    _statusCode=[request responseStatusCode];
    
    if(request.error!=nil){
        _errorCode=request.error.code;
        _errorMessage=@"网络连接失败";
        return NO;
    }
    else {
        NSData* data=[request responseData];
        
        if([data length]<2){
            _errorCode=900;
            _errorMessage=@"网络连接失败";
            return NO;
        }

        id jsonMap  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        if([jsonMap isKindOfClass:[NSDictionary class]]){
            NSDictionary* dict=(NSDictionary*)jsonMap;
            id resultObj=[dict objectForKey:@"result"];
            if(resultObj!=nil){
                BOOL ret=NO;
                if([resultObj isKindOfClass:[NSString class]])
                    ret=([resultObj isEqualToString:@"success"] || [resultObj isEqualToString:@"true"]);
                else
                    ret=[resultObj boolValue];
                if(!ret){
                    _errorCode=[[dict objectForKey:@"code"] intValue];
                    _errorMessage=[dict objectForKey:@"message"];
                }
            }
        }
        return (_errorCode==0);
    }
}

-(int)statusCode{
    return _statusCode;
}

-(int)errorCode{
    return _errorCode;
}

-(NSString*)errorMessage{
    return _errorMessage;
}


@end
