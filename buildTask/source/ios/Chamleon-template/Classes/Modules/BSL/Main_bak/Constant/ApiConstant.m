//
//  ApiConstant.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "ApiConstant.h"

@implementation ApiConstant

+(ApiConstant*)defaultConstant{
    static ApiConstant *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ApiConstant alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self=[super init];
    self.appKey=@"9ac10bdf29e6cf120294703c95a60878";
    self.serverURLHost = @"115.28.1.119";
//    self.pushServerURL = @"192.168.11.18:18860";
//    self.receptHostURL = @"imap.csair.com";
    return self;
}

@end
