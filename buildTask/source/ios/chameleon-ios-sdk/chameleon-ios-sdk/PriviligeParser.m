//
//  PriviligeParser.m
//  chameleon-ios-sdk
//
//  Created by Fanty on 14-1-9.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "PriviligeParser.h"
#import "PriviligeModel.h"

@implementation PriviligeParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        
        
        NSDictionary* dict=(NSDictionary*)jsonMap;
        
        NSString* rolesTag=[dict objectForKey:@"rolesTag"];
        if([rolesTag isKindOfClass:[NSString class]]){
            RolePriviligeModel* rolePriviligeModel=[[RolePriviligeModel alloc] init];
            rolePriviligeModel.rolesName=rolesTag;
            
            NSMutableArray* list=[[NSMutableArray alloc] initWithCapacity:2];

            NSArray* priviliges=[dict objectForKey:@"priviliges"];
            if([priviliges isKindOfClass:[NSArray class]]){
                for(NSArray* array in priviliges){
                    PriviligeModel* model =[[PriviligeModel alloc] init];
                    model.priviligeActionType=([[array objectAtIndex:0] isEqualToString:@"DELETE"]?PriviligeActionTypeDelete:PriviligeActionTypeGet);
                    
                    model.identifier=[array objectAtIndex:1];
                    
                    model.roleName=[array objectAtIndex:2];
                    
                    [list addObject:model];
                }
            }
            
            rolePriviligeModel.priviliges=list;
            
            result=rolePriviligeModel;
        }

    }
}

@end

