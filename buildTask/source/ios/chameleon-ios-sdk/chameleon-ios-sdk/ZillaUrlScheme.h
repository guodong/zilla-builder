//
//  ZillaUrlScheme.h
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZillaAccessData;
@interface ZillaUrlScheme : NSObject

@property(nonatomic,strong) ZillaAccessData* zillaAccessData;

//应用验证api
-(NSURL*)appCheckClient;

//应用版本检测接口
-(NSURL*)appVersionCheck;

//新版本地址
-(NSURL*)applicationNewVersionLink:(NSString*)downloadUrl;

//应用签到接口
-(NSURL*)deviceRegisterURL;

//登录api
-(NSURL*)loginURL;

//登出api
-(NSURL*)logoutURL;

//附件接口
-(NSURL*)attachmentByIdURL:(NSString*)aId;

//附件接口
-(NSString*)attachmentByAIdWithNoAppKey:(NSString*)aId;


//获取模块列表api
-(NSURL*)moduleListURL;

//获取快照api
-(NSURL*)snapshotURL:(NSString*)identifier version:(NSString*)version;

//消息列表api
-(NSURL*)messageListURL;

//某条消息列表
-(NSURL*)messageByIdURL:(NSString*)messageId;

//回执api
-(NSURL*)receptURL;


//角色授权列表api
-(NSURL*)rolePriviligesURL:(NSString*)accountName;


@end
