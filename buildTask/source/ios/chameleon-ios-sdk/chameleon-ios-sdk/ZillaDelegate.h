//
//  ZillaDelegate.h
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppInfoModel;
@class LoginModel;
@class RoleModel;
@class RolePriviligeModel;

@protocol ZillaDelegate <NSObject>
@optional
//应用验证回调
-(void)authorSuccess;
-(void)authorFailed:(int)statusCode errorMessage:(NSString*)errorMessage;
-(void)authorExpired;

//应用版本回调
-(void)appVersionChecked:(AppInfoModel*)appInfoModel;

-(void)appVersionFailed:(int)statusCode errorMessage:(NSString*)errorMessage;

//设备签到回调
-(void)registerDeviceFinish:(BOOL)success;


//登陆回调
-(void)loginSuccess:(LoginModel*)loginModel;
-(void)loginFailed:(int)statusCode errorMessage:(NSString*)errorMessage;
-(void)loginExpired;

//模块获取回调
-(void)syncModulesSuccess:(NSArray*)list;
-(void)syncModulesFailed:(int)statusCode errorMessage:(NSString*)errorMessage;


//快照获取回调
-(void)snapshotSuccess:(NSArray*)list;
-(void)snapshotFailed:(int)statusCode errorMessage:(NSString*)errorMessage;


//未回执消息获取回调
-(void)syncReceiptsSuccess:(NSArray*)list;
-(void)syncReceiptsFailed:(int)statusCode errorMessage:(NSString*)errorMessage;

//回执调用完成回调
-(void)feedBackCallBack;


//回执权限获取回调
-(void)rolePriviligesListSuccess:(RolePriviligeModel*)rolePriviligeModel;
-(void)rolePriviligesListFailed:(int)statusCode errorMessage:(NSString*)errorMessage;

@end
