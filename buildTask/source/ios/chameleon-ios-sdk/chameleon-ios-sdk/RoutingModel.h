//
//  RoutingModel.h
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutingModel : NSObject

@property(nonatomic,strong) NSString* moduleName;
@property(nonatomic,strong) NSString* moduleIdentifier;
@property(nonatomic,strong) NSString* identifier;

//读入json 数组，解析
-(void)parserWithArray:(NSArray*)array;

//返回pi配度的值
-(int)maxCompareArray:(NSArray*)array;

//返回页面标识，输入参数
-(NSString*)mappingIdentifier:(NSArray*)array outputParams:(NSMutableDictionary*)outputParams;

@end
