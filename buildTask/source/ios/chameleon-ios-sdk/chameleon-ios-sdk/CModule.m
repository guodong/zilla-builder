//
//  CModule.m
//  Chamleon-template
//
//  Created by Fanty on 13-11-28.
//
//

#import "CModule.h"
#import "CApplication.h"

@implementation CModule
@synthesize identifier, name;


- (BOOL)application:(CApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;{
    NSLog(@"%@ launched.", [[self class] description]);
    return YES;
}

-(UIViewController*)startMainViewControllerWithParams:(NSDictionary*)dict{
    return nil;
}

-(NSString*)routingJson{
    NSString* path=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@.bundle/data/routing",self.identifier] ofType:@"json"];
    
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

-(UIViewController*)viewControllerWithPageIdentifier:(NSString*)pageIdentifier{
    Class class=NSClassFromString(pageIdentifier);
    if(class!=nil){
        UIViewController* viewController=[[class alloc] init];
        return viewController;
    }
    return nil;
}

-(void)startDispatchAsync{
    
}

-(void)finishDispatchAsync{
    
}


@end
