//
//  DBStorageHelper.h
//  IsolatedStorageDB
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChamleonDB;

@interface DBStorageData:NSObject

@property(nonatomic,strong) NSString* identifier;

//打开数据库
-(ChamleonDB*)openDatabase;

//是否已经创建数据库
-(BOOL)existsDatabase;

//移除数据库
-(void)removeDatabase;

//更新数据库
-(void)updateDatabase;

//判断数据库版本
-(NSString*)newVersion;

//当前使用的数据库版本
-(NSString*)currentVersion;


@end


@interface DBStorageHelper : NSObject

//单例
+(id)standandStorage;

//读取模块的配置信息，配置数据库
-(void)configStorageWithModule:(NSString*)moduleIdentifier;

//传入模块标识，输出模块数据库封装对象
-(DBStorageData*)storageDataWithModuleIdentifier:(NSString*)moduleIdentifier;

@end

