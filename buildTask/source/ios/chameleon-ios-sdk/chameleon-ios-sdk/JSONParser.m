//
//  JsonParser.m
//
//
//  Created by fanty on 13-4-28.
//  Copyright (c) 2013年 . All rights reserved.
//

#import "JSONParser.h"
#import "ChamleonJSON.h"

@implementation JSONParser

-(void)parse:(NSData*)data{
    result=nil;
    if([data length]>2){
        id jsonMap  = [data JSONObject];
        [self onParser:jsonMap];
    }
}

-(void)onParser:(id)jsonMap{
    
}

-(id)getResult{
    return result;
}

@end
