//
//  CubeModel.h
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    CubeMoudleStatusNone=0,
    CubeMoudleStatusInstalling,
    CubeMoudleStatusCanUpdate,
    CubeMoudleStatusUpdating,
    CubeMoudleStatusFinish
}CubeMoudleStatus;


//模块模型
@interface CubeModel:NSObject

@property(nonatomic,strong) NSString* icon;
@property(nonatomic,assign) BOOL isAutoShow;
@property(nonatomic,assign) BOOL autoDownload;

@property(nonatomic,assign) int build;

@property(nonatomic,strong) NSString* version;

@property(nonatomic,strong) NSString* category;

@property(nonatomic,assign) BOOL hidden;

@property(nonatomic,strong) NSString* name;

@property(nonatomic,strong) NSString* timeUnit;

@property(nonatomic,strong) NSString* bundle;

@property(nonatomic,strong) NSString* releaseNote;

@property(nonatomic,strong) NSString* identifier;

@property(nonatomic,strong) NSString* showIntervalTime;

@property(nonatomic,assign) int sortingWeight;

@property(nonatomic,strong) NSString* local;

@property(nonatomic,assign) BOOL hasPrivileges;

@property(nonatomic,strong) NSString* privileges;

@property(nonatomic,assign) long downloadedProcess;
@property(nonatomic,assign) long downloadedTotalCount;

@property(nonatomic,assign) CubeMoudleStatus status;

@property(nonatomic,assign) int unReadCount;

@property(nonatomic,assign) BOOL moduleBadge;

@end
