//
//  LoginParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-29.
//
//

#import "LoginParser.h"
#import "LoginModel.h"

@implementation LoginParser

-(void)onParser:(id)jsonMap{
    
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSDictionary* dict=(NSDictionary*)jsonMap;
        LoginModel* loginModel=[[LoginModel alloc] init];
        
        loginModel.accountToken=[dict objectForKey:@"sessionKey"];
        if([dict objectForKey:@"hasOperation"]!=nil){
            loginModel.hasOperation=[[dict objectForKey:@"hasOperation"] boolValue];
        }
        result=loginModel;
    }
    
}


@end
