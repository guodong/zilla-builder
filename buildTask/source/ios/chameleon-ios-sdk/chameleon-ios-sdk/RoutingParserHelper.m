//
//  RoutingParserHelper.m
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "RoutingParserHelper.h"
#import "ChamleonJSON.h"
#import "RoutingModel.h"

@implementation RoutingParserHelper{
    NSMutableArray* routings;
}

+(RoutingParserHelper *)sharedRouting{
    static RoutingParserHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RoutingParserHelper alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self=[super init];
    if(self){
        routings=[[NSMutableArray alloc] initWithCapacity:1];
    }
    return self;
}

-(void)readConfig:(NSString*)configJSON moduleIdentifier:(NSString*)moduleIdentifier moduleName:(NSString*)moduleName{
    NSArray* array = [configJSON JSONObject];
    if([array isKindOfClass:[NSArray class]]){
        
        for(NSDictionary* dict in array){
            NSString* identifier=[dict objectForKey:@"identifier"];
            NSArray* mappings=[dict objectForKey:@"mappings"];
            if([identifier length]>0 || [mappings count]>0){
                RoutingModel* routing=[[RoutingModel alloc] init];
                routing.identifier=identifier;
                routing.moduleIdentifier=moduleIdentifier;
                routing.moduleName=moduleName;
                [routing parserWithArray:mappings];
                [routings addObject:routing];
            }
        }
    }
}


-(NSString*)moduleIdentifierForLink:(NSString*)urlLink identifier:(NSString*)identifier{
    
    for(RoutingModel* routing in routings){
        if([routing.identifier isEqualToString:identifier]){
            return routing.moduleIdentifier;
        }
    }
    return nil;
}

-(NSString*)pageIdentifierForLink:(NSString*)urlLink identifier:(NSString*)identifier outParams:(NSMutableDictionary*)param{
    for(RoutingModel* routing in routings){
        if([routing.identifier isEqualToString:identifier]){
            NSMutableArray* linkURLs=[[NSMutableArray alloc] initWithCapacity:1];
            NSArray* t=[urlLink componentsSeparatedByString:@"/"];
            for(NSString* link in t){
                if([link length]>0){
                    [linkURLs addObject:link];
                }
            }
            
            return [routing mappingIdentifier:linkURLs outputParams:param];

            
        }
    }
    return nil;
}

/*
-(NSString*)pageIdentifierForLink:(NSString*)urlLink identifier:(NSString*)identifier{
    for(RoutingModel* routing in routings){
        if([routing.identifier isEqualToString:identifier]){
            return routing.moduleName;
        }
    }
    return nil;

}
 */














-(NSString*)html5ModuleIdentifierForLink:(NSString*)urlLink{
    NSArray* t=[urlLink componentsSeparatedByString:@"/"];
    for(NSString* link in t){
        if([link length]>0){
            return link;
        }
    }
    return nil;
}

-(NSString*)pageForLink:(NSString*)urlLink outParams:(NSMutableDictionary*)param{
    
    NSMutableArray* linkURLs=[[NSMutableArray alloc] initWithCapacity:1];
    NSArray* t=[urlLink componentsSeparatedByString:@"/"];
    for(NSString* link in t){
        if([link length]>0){
            [linkURLs addObject:link];
        }
    }

    RoutingModel* model=nil;
    int currentMaxIndex=0;
    for(RoutingModel* _model in routings){
        int _maxIndex=[_model maxCompareArray:linkURLs];
        if(_maxIndex>currentMaxIndex){
            model=_model;
            currentMaxIndex=_maxIndex;
        }
    }
    if(model!=nil){
        return [model mappingIdentifier:linkURLs outputParams:param];
    }
    return model.moduleName;

}



@end
