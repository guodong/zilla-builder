//
//  MessageModel.h
//  Message
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModel : NSObject

@property(nonatomic,strong) NSString* messageId;

@property(nonatomic,strong) NSString* title;

@property(nonatomic,strong) NSString* content;

@property(nonatomic,strong) NSString* moduleIdentifer;

@property(nonatomic,strong) NSString* moduleName;

@property(nonatomic,strong) NSString* announceId;

@property(nonatomic,assign) BOOL moduleBadge;     //是否需要显示icon

@property(nonatomic,assign) BOOL busiDetail;       //是否做跳转详细页

@property(nonatomic,strong) NSString* messageType;   //SYS  系统  //MODULE 模块

@property(nonatomic,strong) NSDate* receiveTime;

@property(nonatomic,assign) BOOL isRead;             //判断是否已读

@property(nonatomic,strong) NSString* moduleUrl;

@property(nonatomic,strong) NSString* securityKey;    // securityKey值只能为roles或者privilege  roles表示角色变更，privilege表示模块授权变更

@end



