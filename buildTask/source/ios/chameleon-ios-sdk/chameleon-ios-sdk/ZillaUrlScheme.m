//
//  ZillaUrlScheme.m
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "ZillaUrlScheme.h"
#import "ZillaAccessData.h"
#import "UIDevice+IdentifierAddition.h"

@implementation ZillaUrlScheme

-(NSURL*)appCheckClient{
    NSString* url=[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/apps/%@/%@/%@/validate",self.zillaAccessData.urlSchemeSuffix,@"ios",[[NSBundle mainBundle] bundleIdentifier],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] ];
    return [NSURL URLWithString:url];
}

-(NSURL*)appVersionCheck{
    NSString* url=[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/update/ios/%@/?ts=%f&appKey=%@",self.zillaAccessData.urlSchemeSuffix,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"], [NSDate timeIntervalSinceReferenceDate],self.zillaAccessData.appKey];
    return [NSURL URLWithString:url];
    
}

-(NSURL*)attachmentByIdURL:(NSString*)aId{
    
    NSString* build=[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/files/%@?appKey=%@",self.zillaAccessData.urlSchemeSuffix,aId,self.zillaAccessData.appKey];
    return [NSURL URLWithString:build];
}

-(NSString*)attachmentByAIdWithNoAppKey:(NSString*)aId{
    NSString* build=[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/files/%@",self.zillaAccessData.urlSchemeSuffix,aId];
    return [NSURL URLWithString:build];

}


-(NSURL*)applicationNewVersionLink:(NSString*)downloadUrl{
    NSString *url = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@", [NSString stringWithFormat:@"%@%@appKey%@%@",downloadUrl,@"%3F",@"%3D",self.zillaAccessData.appKey]];
    return [NSURL URLWithString:url];
}

-(NSURL*)deviceRegisterURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/push/api/checkinservice/checkins",self.zillaAccessData.urlSchemeSuffix]];
}

-(NSURL*)loginURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/system/api/system/mobile/accounts/login",self.zillaAccessData.urlSchemeSuffix]];
}

-(NSURL*)logoutURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/system/api/system/mobile/accounts/logout",self.zillaAccessData.urlSchemeSuffix]];

}

-(NSURL*)moduleListURL{
    
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/apps/modules/%@",self.zillaAccessData.urlSchemeSuffix,self.zillaAccessData.appToken]];
    
    //return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/ios/%@/%@/?username=%@&sessionKey=%@&timeStamp=%f",self.serverURLHost,[[NSBundle mainBundle] bundleIdentifier],[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],username,sessionKey,[NSDate timeIntervalSinceReferenceDate]]];
    
    
}

-(NSURL*)snapshotURL:(NSString*)identifier version:(NSString*)version{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/widget/%@/%@/snapshot?appKey=%@",self.zillaAccessData.urlSchemeSuffix,identifier,version,self.zillaAccessData.appKey]];
}


-(NSURL*)messageListURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/push/api/push-msgs/none-receipts/%@/%@",self.zillaAccessData.urlSchemeSuffix,[[UIDevice currentDevice] uniqueDeviceIdentifier],self.zillaAccessData.appKey]];
}

-(NSURL*)receptURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/push/api/receipts",self.zillaAccessData.urlSchemeSuffix]];
}

-(NSURL*)messageByIdURL:(NSString*)messageId{
    
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/push/api/push-msgs/%@/%@",self.zillaAccessData.urlSchemeSuffix,self.zillaAccessData.appKey,messageId]];
    
}

//角色授权列表api
-(NSURL*)rolePriviligesURL:(NSString*)accountName{
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/mam/api/mam/clients/apps/%@/%@/auth?appKey=%@",self.zillaAccessData.urlSchemeSuffix,[[NSBundle mainBundle] bundleIdentifier],accountName,self.zillaAccessData.appKey]];
}


@end
