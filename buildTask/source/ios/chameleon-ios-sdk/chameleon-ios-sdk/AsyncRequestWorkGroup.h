//
//  AsyncRequestWorkGroup.h
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-27.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AsyncRequestWorkGroupBlock)();

@class ChamleonHTTPRequest;
@class HttpCheckOperator;
@class JSONParser;


//异步解析类：  从http ->  检测错误器   ->json解析 -> 结果
@interface AsyncRequestWorkGroup : NSObject
@property(nonatomic,strong) ChamleonHTTPRequest* request;
@property(nonatomic,strong) HttpCheckOperator* checkOperator;
@property(nonatomic,strong) JSONParser* parser;
@property (copy, nonatomic) AsyncRequestWorkGroupBlock finishBlock;

//取消
-(void)cancel;

//执行
-(void)start;

//成功后，返回的对象
-(id)result;

//状态码
-(int)statusCode;

//错误码
-(int)errorCode;

//错误信息
-(NSString*)errorMessage;

//返回http data
-(NSData*)responseData;

@end


