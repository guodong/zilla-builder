//
//  AppValidateParser.m
//  Main
//
//  Created by Fanty on 13-12-18.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "AppValidateParser.h"
#import "AppValidate.h"
@implementation AppValidateParser

-(void)onParser:(id)jsonMap{
    
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSDictionary* dict=(NSDictionary*)jsonMap;
        AppValidate* model=[[AppValidate alloc] init];
        
        model.token=[dict objectForKey:@"token"];
        NSDateFormatter* formmater=[[NSDateFormatter alloc] init];
        [formmater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        model.expired=[formmater dateFromString:[dict objectForKey:@"expired"]];
        
        result=model;
    }
    
}


@end
