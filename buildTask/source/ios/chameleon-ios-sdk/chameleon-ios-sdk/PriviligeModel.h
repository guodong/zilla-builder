//
//  PriviligeModel.h
//  chameleon-ios-sdk
//
//  Created by Fanty on 14-1-9.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    PriviligeActionTypeGet=0,
    PriviligeActionTypeDelete
}PriviligeActionType;

@interface PriviligeModel : NSObject

@property(nonatomic,assign) PriviligeActionType priviligeActionType;
@property(nonatomic,strong) NSString* identifier;
@property(nonatomic,strong) NSString* roleName;

@end


@interface RolePriviligeModel:NSObject

@property(nonatomic,strong) NSString* rolesName;

@property(nonatomic,strong) NSArray* priviliges;

@end




