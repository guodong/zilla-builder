//
//  MappingModel.m
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "MappingModel.h"

@implementation MappingModel

-(void)parserWithKey:(NSString*)key value:(NSString*)value{
    self.pageIdentifier=value;
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
    NSMutableArray* array2=[[NSMutableArray alloc] initWithCapacity:1];
    NSArray* t=[key componentsSeparatedByString:@"/"];
    for(NSString* link in t){
        if([link length]>0){
            if([link rangeOfString:@"{"].length>0){
                [array2 addObject:[[link stringByReplacingOccurrencesOfString:@"{" withString:@""] stringByReplacingOccurrencesOfString:@"}" withString:@""]];
            }
            else{
                [array addObject:link];
            }
        }
    }
    linkURLs=array;
    paramURLs=array2;
}

-(int)compareLinks:(NSArray*)_linkURLS{
    int maxCount=0;
    for(int i=0;i<[linkURLs count];i++){
        if(i>[_linkURLS count]-1)break;
        NSString* origin=[linkURLs objectAtIndex:i];
        NSString* newest=[_linkURLS objectAtIndex:i];
        if([origin isEqualToString:newest]){
            maxCount++;
        }
        else{
            break;
        }
    }
    if(maxCount==[_linkURLS count])
        maxCount=99999;
    return maxCount;
}

-(NSDictionary*)params:(NSArray*)_linkURLS{
    int maxCount=0;
    for(int i=0;i<[linkURLs count];i++){
        if(i>[_linkURLS count]-1)break;
        NSString* origin=[linkURLs objectAtIndex:i];
        NSString* newest=[_linkURLS objectAtIndex:i];
        if([origin isEqualToString:newest]){
            maxCount++;
        }
        else{
            break;
        }
    }
    int index=0;
    NSMutableDictionary* dict=[[NSMutableDictionary alloc] initWithCapacity:1];
    for(int i=maxCount;i<[_linkURLS count];i++){
        
        NSString* key=[NSString stringWithFormat:@"mapping_ios_param_%d",index];
        if(index<[paramURLs count]){
            key=[paramURLs objectAtIndex:index];
        }
        [dict setObject:[_linkURLS objectAtIndex:i] forKey:key];
        
        index++;
    }
    return dict;
}


@end
