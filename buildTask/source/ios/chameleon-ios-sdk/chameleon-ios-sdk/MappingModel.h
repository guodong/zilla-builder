//
//  MappingModel.h
//  ChamleonSDK
//
//  Created by Fanty on 13-12-24.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MappingModel : NSObject{
    NSArray* linkURLs;
    NSArray* paramURLs;
}
@property(nonatomic,strong) NSString* pageIdentifier;

//解析
-(void)parserWithKey:(NSString*)key value:(NSString*)value;

//返回pi配度
-(int)compareLinks:(NSArray*)_linkURLS;

//形参格式
-(NSDictionary*)params:(NSArray*)_linkURLS;

@end
