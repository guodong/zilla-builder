//
//  ApiConstant.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "ZillaAccessData.h"

@implementation ZillaAccessData

+(ZillaAccessData*)activeSession{
    static ZillaAccessData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ZillaAccessData alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;

}


@end
