//
//  MessageParser.m
//  zilla-ios-sdk
//
//  Created by Fanty on 13-12-30.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "MessageParser.h"
#import "MessageModel.h"

@implementation MessageParser

-(void)onParser:(id)jsonMap{
    
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        
        NSDictionary* dict=jsonMap;
        MessageModel* model=[[MessageModel alloc] init];
        id obj=[dict objectForKey:@"id"];
        if(obj!=nil)
            model.messageId=obj;
        
        obj=[dict objectForKey:@"title"];
        if(obj!=nil)
            model.title=obj;
        
        obj=[dict objectForKey:@"content"];
        if(obj!=nil)
            model.content=obj;
        
        obj=[dict objectForKey:@"messageType"];
        if(obj!=nil)
            model.messageType=obj;
        
        obj=[dict objectForKey:@"extras"];
        if(obj!=nil && [obj isKindOfClass:[NSDictionary class]]){
            NSDictionary* extras=obj;
            
            obj=[extras objectForKey:@"moduleIdentifer"];
            if(obj!=nil)
                model.moduleIdentifer=obj;
            
            obj=[extras objectForKey:@"announceId"];
            if(obj!=nil)
                model.announceId=obj;
            
            obj=[extras objectForKey:@"moduleBadge"];
            if(obj!=nil)
                model.moduleBadge=[obj boolValue];
            
            obj=[extras objectForKey:@"busiDetail"];
            if(obj!=nil)
                model.busiDetail=[obj boolValue];
            
            obj=[extras objectForKey:@"moduleName"];
            if(obj!=nil)
                model.moduleName=obj;
            
            obj=[extras objectForKey:@"securityKey"];
            if(obj!=nil)
                model.securityKey=obj;

            obj=[extras objectForKey:@"moduleUrl"];
            if(obj!=nil)
                model.moduleUrl=obj;
            
        }
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
        [array addObject:model];
        result=array;
    }
    
    else if([jsonMap isKindOfClass:[NSArray class]]){
        NSArray* list=jsonMap;
        if([list count]>0){
            NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
            for(NSDictionary* dict in list){
                MessageModel* model=[[MessageModel alloc] init];
                id obj=[dict objectForKey:@"id"];
                if(obj!=nil)
                    model.messageId=obj;
                
                obj=[dict objectForKey:@"title"];
                if(obj!=nil)
                    model.title=obj;
                
                obj=[dict objectForKey:@"content"];
                if(obj!=nil)
                    model.content=obj;
                
                obj=[dict objectForKey:@"messageType"];
                if(obj!=nil)
                    model.messageType=obj;
                
                obj=[dict objectForKey:@"extras"];
                if(obj!=nil && [obj isKindOfClass:[NSDictionary class]]){
                    NSDictionary* extras=obj;
                    
                    obj=[extras objectForKey:@"moduleIdentifer"];
                    if(obj!=nil)
                        model.moduleIdentifer=obj;
                    
                    obj=[extras objectForKey:@"announceId"];
                    if(obj!=nil)
                        model.announceId=obj;
                    
                    obj=[extras objectForKey:@"moduleBadge"];
                    if(obj!=nil)
                        model.moduleBadge=[obj boolValue];
                    
                    obj=[extras objectForKey:@"busiDetail"];
                    if(obj!=nil)
                        model.busiDetail=[obj boolValue];
                    
                    obj=[extras objectForKey:@"moduleName"];
                    if(obj!=nil)
                        model.moduleName=obj;
                    
                    obj=[extras objectForKey:@"moduleUrl"];
                    if(obj!=nil)
                        model.moduleUrl=obj;
                    
                    obj=[extras objectForKey:@"securityKey"];
                    if(obj!=nil)
                        model.securityKey=obj;
                }
                
                [array addObject:model];
            }
            
            result=array;
        }
    }
}


@end
