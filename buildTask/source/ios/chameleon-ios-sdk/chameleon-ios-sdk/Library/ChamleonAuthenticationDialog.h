//
//  ChamleonAuthenticationDialog.h
//  Part of ChamleonHTTPRequest -> http://allseeing-i.com/ChamleonHTTPRequest
//
//  Created by Ben Copsey on 21/08/2009.
//  Copyright 2009 All-Seeing Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class ChamleonHTTPRequest;

typedef enum _ChamleonAuthenticationType {
	ChamleonStandardAuthenticationType = 0,
    ChamleonProxyAuthenticationType = 1
} ChamleonAuthenticationType;

@interface ChamleonAutorotatingViewController : UIViewController
@end

@interface ChamleonAuthenticationDialog : ChamleonAutorotatingViewController <UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource> {
	ChamleonHTTPRequest *request;
	ChamleonAuthenticationType type;
	UITableView *tableView;
	UIViewController *presentingController;
	BOOL didEnableRotationNotifications;
}
+ (void)presentAuthenticationDialogForRequest:(ChamleonHTTPRequest *)request;
+ (void)dismiss;

@property (retain) ChamleonHTTPRequest *request;
@property (assign) ChamleonAuthenticationType type;
@property (assign) BOOL didEnableRotationNotifications;
@property (retain, nonatomic) UIViewController *presentingController;
@end
