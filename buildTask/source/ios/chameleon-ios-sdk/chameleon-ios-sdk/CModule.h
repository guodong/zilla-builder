//
//  CModule.h
//  Chamleon-template
//
//  Created by Fanty on 13-11-28.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CApplication;

//
// BSL Module, Devide iOS App into modules.
// CModule is awared of the full application life cycle
//
@interface CModule : NSObject<UIApplicationDelegate>

//module infomation
@property(nonatomic, strong)NSString *identifier;
@property(nonatomic, strong)NSString *name;
@property(nonatomic,assign) BOOL splash_start;
@property(nonatomic,strong) NSMutableArray* dependOn;

#pragma mark - Module Lifecycle
//this method will be called when the application finish launch, the module should initialize itself
- (BOOL)application:(CApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

//the ViewController represent for this Module, can be nil
-(UIViewController*)startMainViewControllerWithParams:(NSDictionary*)dict;

-(UIViewController*)viewControllerWithPageIdentifier:(NSString*)pageIdentifier;

-(NSString*)routingJson;

-(void)startDispatchAsync;

-(void)finishDispatchAsync;

@end

