//
//  LandScroller.m
//  Chamleon-template
//
//  Created by Fanty on 13-12-16.
//
//

#import "LandScroller.h"
#import "ImageLoadingDownloadedView.h"
#import <chameleon-ios-sdk/ZillaAccessData.h>
#import <chameleon-ios-sdk/ZillaUrlScheme.h>

#import <MainModuleSDK/DataCenter.h>
@implementation LandScroller

- (id)init{
    self = [super init];
    if (self) {
        // Initialization code
        self.userInteractionEnabled=YES;
        self.image=[UIImage imageNamed:@"Module.bundle/images/introduce_detail_snapshotbg.png"];
        CGRect frame=self.frame;
        frame.size=self.image.size;
        self.frame=frame;
        
        scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 30.0f, frame.size.width, frame.size.height-50.0f)];
        scrollView.pagingEnabled=YES;
        scrollView.showsVerticalScrollIndicator=NO;
        [self addSubview:scrollView];
        
    }
    return self;
}

-(void)setFrame:(CGRect)value{
    [super setFrame:value];
    scrollView.frame=CGRectMake(0.0f, 30.0f, value.size.width, value.size.height-50.0f);
}

-(void)setImages:(NSArray*)array{
    float left=0.0f;

    
    ZillaUrlScheme* zillaUrlScheme=[[ZillaUrlScheme alloc] init];
    zillaUrlScheme.zillaAccessData=[ZillaAccessData activeSession];
    
    for(NSString* val in array){
        ImageLoadingDownloadedView* imgView=[[ImageLoadingDownloadedView alloc] initWithFrame:CGRectMake(left, 0.0f, scrollView.frame.size.width, scrollView.frame.size.height)];
        [imgView setUrl:[[zillaUrlScheme attachmentByIdURL:val] absoluteString]];
        [scrollView addSubview:imgView];
        imgView.contentMode=UIViewContentModeScaleAspectFit;
        left+=scrollView.frame.size.width;
    }
    scrollView.contentSize=CGSizeMake(left, scrollView.frame.size.height);
}

@end
