//
//  IPadNavViewController.m
//  Module
//
//  Created by Fanty on 13-12-23.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "IPadNavViewController.h"
#import "BSLCubeConstant.h"

@interface IPadNavViewController ()

@end

@implementation IPadNavViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    CGRect rect=self.view.frame;
    rect.size=IPAD_SIZE;
    self.view.frame=rect;
    
    UIImageView* imgView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/images/line_shadow.png"]];
    [self.view addSubview:imgView];
    
    rect=imgView.frame;
    rect.origin.x=-rect.size.width;
    rect.size.height=self.view.frame.size.height;
    imgView.frame=rect;

    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
