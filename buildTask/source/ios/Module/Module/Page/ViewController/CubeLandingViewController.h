//
//  CubeLandingViewController.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-6.
//
//

#import <MainModuleSDK/CViewController.h>
@class CubeModel;
@class ImageDownloadedView;
@class LandScroller;
@class Zilla;
@interface CubeLandingViewController : CViewController{
    int type;
    BOOL local;
    ImageDownloadedView* imageView;
    
    UILabel* titleView;
    UILabel* vertionView;
    UILabel* descView;
    UIButton* confirmButton;
    
    UILabel* landDescView;
    LandScroller* landScroller;
    
    UIProgressView* progressView;
    
    Zilla* zillaSDK;
}
@property(nonatomic,strong) NSString* identifier;

@end
