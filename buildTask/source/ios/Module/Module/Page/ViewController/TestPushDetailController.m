//
//  TestPushDetailController.m
//  Module
//
//  Created by Fanty on 14-1-2.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "TestPushDetailController.h"

@interface TestPushDetailController ()
-(void)btnClose;
@end

@implementation TestPushDetailController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor grayColor];
    
    UIButton* button=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame=CGRectMake(10.0f, 10.0f, 100.0f, 50.0f);
    [button setTitle:@"关闭" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnClose) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UILabel* label=[[UILabel alloc] initWithFrame:self.view.bounds];
    label.textAlignment=NSTextAlignmentCenter;
    label.numberOfLines=0;
    label.backgroundColor=[UIColor clearColor];
    label.font=[UIFont systemFontOfSize:20.0f];
    [self.view addSubview:label];
    
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:2];
    [str appendString:@"测试推送详细页\n通过推送出的viewcontroller:\n 参数:\rn"];
    [self.params enumerateKeysAndObjectsUsingBlock:^(id key,id obj,BOOL*stop){
        [str appendFormat:@"param:%@  = value:%@\n",key,obj];
    }];
    label.text=str;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnClose{
    if(self.navigationController!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissModalViewControllerAnimated:YES];
}

@end
