//
//  CubeModuleParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubeModuleParser.h"

#import <chameleon-ios-sdk/CubeModel.h>
#import "CubesModel.h"
#import "DataCenter+Cube.h"
#import "IsolatedStorageFile+CubeLogic.h"

@implementation CubeModuleParser

+(NSString*)serialize:(CubeModel*)model  showOutputIcon:(BOOL)showOutputIcon{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    [str appendFormat:@"\"category\":\"%@\",",[model.category stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];

    if(showOutputIcon)
        [str appendFormat:@"\"icon\":\"%@\",",[IsolatedStorageFile outputIcon:model]];
    else
        [str appendFormat:@"\"icon\":\"%@\",",[model.icon stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"isAutoShow\":%@,",(model.isAutoShow?@"true":@"false") ];
        [str appendFormat:@"\"autoDownload\":%@,",(model.autoDownload?@"true":@"false") ];
        if(model.hasPrivileges)
            [str appendString:@"\"privileges\":\"\"," ];
    }
    else{
        if(model.hasPrivileges){
            [str appendString:@"\"privileges\":true," ];
        }
        else{
            /*
            RoleModel* roles=[DataCenter defaultCenter].roles;
            
            for(NSString* identifier in roles.identifierArray){
                if([identifier isEqualToString:model.identifier]){
                    [str appendString:@"\"privileges\":true," ];
                    break;
                }
            }
             */
        }
    }
    [str appendFormat:@"\"build\":%d,",model.build ];
    [str appendFormat:@"\"version\":\"%@\",",[model.version stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    [str appendFormat:@"\"hidden\":%@,",(model.hidden?@"true":@"false") ];
    [str appendFormat:@"\"name\":\"%@\",",[model.name stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    if(!showOutputIcon){
        [str appendFormat:@"\"timeUnit\":\"%@\",",[model.timeUnit stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
        [str appendFormat:@"\"bundle\":\"%@\",",[model.bundle stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    }
    [str appendFormat:@"\"releaseNote\":\"%@\",",[model.releaseNote stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if([model.local length]<1)
        [str appendString:@"\"local\":\"\","];
    else
        [str appendFormat:@"\"local\":\"%@\",",[model.local stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    [str appendFormat:@"\"identifier\":\"%@\",",[model.identifier stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"showIntervalTime\":\"%@\",",[model.showIntervalTime stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    }
    [str appendFormat:@"\"sortingWeight\":%d,",model.sortingWeight ];
    
    if(!showOutputIcon){
        [str appendFormat:@"\"status\":%d,",model.status ];
    
        [str appendFormat:@"\"unReadCount\":%d,",model.unReadCount ];
        
        [str appendFormat:@"\"moduleBadge\":%@",(model.moduleBadge?@"true":@"false") ];

    }
    else{
        [str appendFormat:@"\"updatable\":%@,",(model.status==CubeMoudleStatusCanUpdate?@"true":@"false") ];
        
        
        [str appendFormat:@"\"msgCount\":%d",model.unReadCount ];
    }
    
    
    [str appendString:@"}"];

    return str;
}

+(NSString*)parserJSONFromArray:(NSArray*)array showOutputIcon:(BOOL)showOutputIcon{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{\"result\":\"success\",\"modules\":["];
    [array enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL* stop){
        @autoreleasepool {
            CubeModel* model=(CubeModel*)obj;
            [str appendString:[CubeModuleParser serialize:model showOutputIcon:showOutputIcon]];
            if(index<[array count]-1)
                [str appendString:@","];
        }
    }];
    [str appendString:@"]}"];
    
    return str;
}

/*
+(NSString*)handleModuleToJson:(CubeModuleJsonSerial*)list{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    for(int index=0;index<[list.allKeys count];index++){
        
        @autoreleasepool {
            NSString* cubeModuleKey=[list.allKeys objectAtIndex:index];

            NSArray* array=[list objectForKey:cubeModuleKey];
            [str appendFormat:@"\"%@\"",cubeModuleKey];
            [str appendString:@":["];
            

            [array enumerateObjectsUsingBlock:^(id _obj,NSUInteger _index,BOOL*_stop){
                CubeModel* model=(CubeModel*)_obj;
                [str appendString:[CubeModuleParser serialize:model showOutputIcon:YES]];
                if(_index<[array count]-1)
                    [str appendString:@","];
            }];
            
            [str appendString:@"]"];
            
            if (index< [list.allKeys count]-1){
                //不是最后一个元素，得加上逗号
                [str appendString:@","];
            }
            
        }
    }
    
    [str appendString:@"}"];
    return str;
}
 */

+(NSString*)handleModuleToJson:(NSDictionary*)list{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    for(int index=0;index<[list.allKeys count];index++){
        
        @autoreleasepool {
            NSString* cubeModuleKey=[list.allKeys objectAtIndex:index];
            
            NSArray* array=[list objectForKey:cubeModuleKey];
            [str appendFormat:@"\"%@\"",cubeModuleKey];
            [str appendString:@":["];
            
            
            [array enumerateObjectsUsingBlock:^(id _obj,NSUInteger _index,BOOL*_stop){
                CubeModel* model=(CubeModel*)_obj;
                [str appendString:[CubeModuleParser serialize:model showOutputIcon:YES]];
                if(_index<[array count]-1)
                    [str appendString:@","];
            }];
            
            [str appendString:@"]"];
            
            if (index< [list.allKeys count]-1){
                //不是最后一个元素，得加上逗号
                [str appendString:@","];
            }
            
        }
    }
    
    [str appendString:@"}"];
    return str;
}

@end
