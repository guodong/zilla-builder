//
//  AutoDownloadCheckingOperator.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import <Foundation/Foundation.h>


@class AutoDownloadCheckingOperator;

@protocol AutoDownloadCheckingOperatorDelegate <NSObject>

-(void)autoSaveDidShowAutoDownloading:(AutoDownloadCheckingOperator*)cubeModuleAutoSaveParser message:(NSString*)message;

-(void)autoSaveDidShowUpdating:(AutoDownloadCheckingOperator*)cubeModuleAutoSaveParser message:(NSString*)message;

@end


@interface AutoDownloadCheckingOperator : NSObject{
    NSMutableArray* downloadModels;
}


@property(nonatomic,weak) id<AutoDownloadCheckingOperatorDelegate> delegate;
@property(nonatomic,assign) int autoDownloadedCount;
@property(nonatomic,readonly) NSArray* downloadModels;
@property(nonatomic,strong) NSMutableArray* dependDownloadlist;
@property(nonatomic,strong) NSMutableArray* dependUpdatelist;

//清除自动下载记录
-(void)clear;

//开始检测
-(void)start;

//依赖列表
-(NSArray*)dependsList:(NSString*)identifier;


@end
