//
//  CubeMessageParser.m
//  Module
//
//  Created by Fanty on 14-1-13.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "CubeMessageParser.h"
#import "CubesModel.h"
#import "DataCenter+Cube.h"
#import <chameleon-ios-sdk/CubeModel.h>
#import <chameleon-ios-sdk/MessageModel.h>

@implementation CubeMessageParser


+(NSDictionary*)messageListMap:(NSArray*)list{
    
    NSMutableDictionary* dict=[[NSMutableDictionary alloc] initWithCapacity:1];
    
    for(MessageModel* model in list){
        MessageGroupModel* messageGroupModel=[dict objectForKey:model.moduleIdentifer];
        NSMutableArray* messageList=(NSMutableArray*)messageGroupModel.messageList;
        if(messageGroupModel==nil){
            messageGroupModel=[[MessageGroupModel alloc] init];
            messageGroupModel.moduleIdentifer=model.moduleIdentifer;
            NSString* name=[[DataCenter defaultCenter] finidServiceCubeModule:model.moduleIdentifer].name;
            if([name length]<1)
                name=model.moduleName;
            messageGroupModel.moduleName=name;
            
            messageList=[[NSMutableArray alloc] initWithCapacity:2];
            messageGroupModel.messageList=messageList;
            
            [dict setObject:messageGroupModel forKey:model.moduleIdentifer];
            
        }
        if(!model.isRead)
            messageGroupModel.unReadCount++;
        messageGroupModel.totalCount++;
        [messageList addObject:model];
    }

    return dict;
}

+(NSString*)serialize:(MessageModel*)model{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"{"];
    
    [str appendFormat:@"\"messageId\":\"%@\",",[model.messageId stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    
    [str appendFormat:@"\"title\":\"%@\",",[model.title stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    [str appendFormat:@"\"content\":\"%@\",",[model.content stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    [str appendFormat:@"\"messageType\":\"%@\",",[model.messageType stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];

    
    unsigned long long t=(unsigned long long)([model.receiveTime timeIntervalSince1970]*1000);
    
    [str appendFormat:@"\"sendtime\":%lld,",t];
    
    [str appendFormat:@"\"hasread\":%@",(model.isRead?@"true":@"false")];

    [str appendString:@"}"];

    return str;
}

+(NSString*)parserJSONFromArray:(NSArray*)array{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"["];
    [array enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL* stop){
        @autoreleasepool {
            MessageModel* model=(MessageModel*)obj;
            [str appendString:[CubeMessageParser serialize:model]];
            if(index<[array count]-1)
                [str appendString:@","];
        }
    }];
    [str appendString:@"]"];
    
    return str;
}

+(NSString*)handleModuleToJson:(NSDictionary*)list{
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"["];
    
    for(int index=0;index<[list.allKeys count];index++){
        
        @autoreleasepool {
            NSString* key=[list.allKeys objectAtIndex:index];
            MessageGroupModel* model=[list objectForKey:key];
            
            NSArray* array=model.messageList;
            [str appendString:@"{"];
            [str appendFormat:@"\"identifier\":\"%@\",",[model.moduleIdentifer stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
            [str appendFormat:@"\"moduleName\":\"%@\",",[model.moduleName stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
            [str appendFormat:@"\"count\":%d,",model.totalCount];
            [str appendFormat:@"\"unread\":%d,",model.unReadCount];
            [str appendString:@"\"message\":"];
            [str appendString:[CubeMessageParser parserJSONFromArray:array]];
            [str appendString:@"}"];

            if (index< [list.allKeys count]-1){
                //不是最后一个元素，得加上逗号
                [str appendString:@","];
            }
            
        }
    }
    
    [str appendString:@"]"];
    return str;
}

@end
