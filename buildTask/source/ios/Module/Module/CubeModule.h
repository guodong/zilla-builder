//
//  MainModule.h
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import <chameleon-ios-sdk/CModule.h>
#import "CubeModuleManager.h"
#import "MessageManager.h"
#import "AutoDownloadCheckingOperator.h"
#import "AccountManager.h"


@interface CubeModule : CModule{
    CubeModuleManager* cubeModuleManager;
    AutoDownloadCheckingOperator* autoDownloadCheckingOperator;
    MessageManager* messageManager;
    AccountManager* accountManager;
    NSString* apnsMessageId;
    BOOL isActive;
    BOOL  firstSyncNotification;
}
@property(nonatomic,readonly) CubeModuleManager* cubeModuleManager;
@property(nonatomic,readonly) AccountManager* accountManager;
@property(nonatomic,readonly) AutoDownloadCheckingOperator* autoDownloadCheckingOperator;
@property(nonatomic,readonly) MessageManager* messageManager;


-(void)syncModuleList;

@end
