//
//  DependsOnParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-9.
//
//

#import "DependsOnParser.h"

#import "CubesModel.h"

@implementation DependsOnParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSDictionary class]]){
        NSDictionary* obj =[jsonMap objectForKey:@"dependencies"];
        
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
        
        [obj enumerateKeysAndObjectsUsingBlock:^(id key,id obj,BOOL*stop){
            
            DependsOnModule* model=[[DependsOnModule alloc] init];
            model.identifier=key;
            model.build=[obj intValue];
            [array addObject:model];
            
            
        }];
        
        result=array;
        
    }

}

@end
