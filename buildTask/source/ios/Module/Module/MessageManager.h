//
//  MessageManager.h
//  Message
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MessageModel;
@interface MessageManager : NSObject

//取消
-(void)cancel;


//更新模块的未读数
-(void)syncUpdateNumber;

//同步所有消息
-(void)syncMessage;


//同步某一条消息
-(void)syncMessageById:(NSString*)messageId routing:(BOOL)routing;

//获取消息分组
-(NSArray*)messageGroupList;

//获取消息列表
-(NSArray*)messageList:(NSString*)moduleIdentifier;


//移除消息
-(void)removeMesageById:(NSString*)messageId;

//移除消息
-(void)removeMessageByGroup:(NSString*)moduleIdentifier;


//设置消息已读
-(void)setIsReadMessageById:(NSString*)messageId;


//获取单条消息模块
-(MessageModel*)messageById:(NSString*)messageId;


//跳转模块，routingByMessageRecord代表是从消息盒子进入
-(void)checkApnsRedirectAndRedirect:(MessageModel*)messageModel routingByMessageRecord:(BOOL)routingByMessageRecord;


@end
