//
//  CubesModel.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import <Foundation/Foundation.h>


//依赖模型
@interface DependsOnModule : NSObject
@property(nonatomic,strong) NSString* identifier;
@property(nonatomic,assign) int build;
@end

//本地映射模型
@interface ModuleMap:NSObject

@property(nonatomic,strong) NSString* identifier;
@property(nonatomic,strong) NSString* icon;

@end

//json出的模块模型
@interface CubeModuleJsonSerial:NSObject
@property(nonatomic,strong) NSMutableArray* allKeys;
@property(nonatomic,strong) NSMutableArray* allValues;

-(NSMutableArray*)objectForKey:(NSString*)key;

-(void)setObject:(NSMutableArray*)list forKey:(NSString*)key;

@end

@interface MessageGroupModel:NSObject

@property(nonatomic,strong) NSString* moduleIdentifer;

@property(nonatomic,strong) NSString* moduleName;

@property(nonatomic,assign) int unReadCount;

@property(nonatomic,assign) int totalCount;


//只在plugin 用到
@property(nonatomic,strong) NSArray* messageList;

@end


