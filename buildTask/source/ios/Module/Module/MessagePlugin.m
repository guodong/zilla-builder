//
//  MessagePlugin.m
//  Module
//
//  Created by Fanty on 14-1-13.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "MessagePlugin.h"
#import "MessageManager.h"
#import <chameleon-ios-sdk/CApplication.h>
#import <chameleon-ios-sdk/MessageModel.h>
#import "DataCenter+Cube.h"
#import "CubeModule.h"
#import "CubeMessageParser.h"

@implementation MessagePlugin


-(void)getAllMessages:(CDVInvokedUrlCommand*)command{
    
    NSString* callbackId=command.callbackId;
    
    
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    NSArray* list=[cubeModule.messageManager messageList:nil];
    NSDictionary* dict=[CubeMessageParser messageListMap:list];
    NSString* result=[CubeMessageParser handleModuleToJson:dict];
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)deleteMessages:(CDVInvokedUrlCommand*)command{
    NSString* callbackId=command.callbackId;
    if([command.arguments count]<1){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

        return;
    }
    NSString* identifier=[command.arguments objectAtIndex:0];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    
    [cubeModule.messageManager removeMessageByGroup:identifier];
    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)deleteMessage:(CDVInvokedUrlCommand*)command{
    NSString* callbackId=command.callbackId;
    if([command.arguments count]<1){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        
        return;
    }
    NSString* messageId=[command.arguments objectAtIndex:0];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    
    [cubeModule.messageManager removeMesageById:messageId];
    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)setMessageRead:(CDVInvokedUrlCommand*)command{
    NSString* callbackId=command.callbackId;
    if([command.arguments count]<1){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        
        return;
    }
    NSString* messageId=[command.arguments objectAtIndex:0];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    
    [cubeModule.messageManager setIsReadMessageById:messageId];
    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

-(void)redirectMessagePage:(CDVInvokedUrlCommand*)command{
    NSString* callbackId=command.callbackId;
    if([command.arguments count]<1){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        
        return;
    }
    NSString* messageId=[command.arguments objectAtIndex:0];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    
    MessageModel* messageModel=[cubeModule.messageManager messageById:messageId];
    
    if([messageModel.messageType isEqualToString:@"SYS"] || [messageModel.messageType isEqualToString:@"SECURITY"]){
    
    }
    else if(messageModel.busiDetail){
        if([[DataCenter defaultCenter] finidInstallCubeModule:messageModel.moduleIdentifer]==nil){
            UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"模块未安装，请下载该模块" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else{
            CubeModule* module=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
            [module.messageManager checkApnsRedirectAndRedirect:messageModel  routingByMessageRecord:YES];
        }
    }

    
    

    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
}



@end
