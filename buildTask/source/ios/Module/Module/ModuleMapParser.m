//
//  ModuleMapParser.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-4.
//
//

#import "ModuleMapParser.h"
#import "CubesModel.h"

@implementation ModuleMapParser

-(void)onParser:(id)jsonMap{
    if([jsonMap isKindOfClass:[NSArray class]]){
        
        NSMutableArray* list=[[NSMutableArray alloc] initWithCapacity:2];
        NSArray* array=jsonMap;
        
        for(NSDictionary* dict in array){
            ModuleMap* model=[[ModuleMap alloc] init];
            model.identifier=[dict objectForKey:@"identifier"];
            model.icon=[dict objectForKey:@"icon"];
            [list addObject:model];
            
        }
        result=list;
    }
}

@end
