//
//  CubeModuleDownloaded.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>

#import "ICubeModule.h"
@class ChamleonHTTPRequest;
@class CubeModel;

//下载工作类
@interface CubeModuleDownloaded : NSObject{
    ChamleonHTTPRequest* request;
    NSTimer* processTimer;
    int retryIndex;
}
@property(nonatomic,strong) NSString* bundle;
@property(nonatomic,strong) CubeModel* cubeModel;
@property(nonatomic,weak) id<ICubeModuleWorkDownloadedAndUnZip> callback;


-(void)cancel;

-(void)start;

@end
