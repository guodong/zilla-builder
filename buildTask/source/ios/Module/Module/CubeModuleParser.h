//
//  CubeModuleParser.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import <Foundation/Foundation.h>

@class CubeModel;
//@class CubeModuleJsonSerial;
//模块json解析
@interface CubeModuleParser:NSObject


+(NSString*)serialize:(CubeModel*)cubeModel showOutputIcon:(BOOL)showOutputIcon;
+(NSString*)parserJSONFromArray:(NSArray*)array showOutputIcon:(BOOL)showOutputIcon;
//+(NSString*)handleModuleToJson:(CubeModuleJsonSerial*)list;
+(NSString*)handleModuleToJson:(NSDictionary*)list;
@end
