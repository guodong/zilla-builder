//
//  CubeMessageParser.h
//  Module
//
//  Created by Fanty on 14-1-13.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MessageModel;
@interface CubeMessageParser : NSObject

+(NSDictionary*)messageListMap:(NSArray*)list;
+(NSString*)serialize:(MessageModel*)messageModel;
+(NSString*)parserJSONFromArray:(NSArray*)array;
+(NSString*)handleModuleToJson:(NSDictionary*)list;

@end
