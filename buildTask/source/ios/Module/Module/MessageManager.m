//
//  MessageManager.m
//  Message
//
//  Created by Fanty on 13-12-17.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "MessageManager.h"
#import <chameleon-ios-sdk/ChamleonSDK.h>
#import <chameleon-ios-sdk/MessageModel.h>
#import <AudioToolbox/AudioToolbox.h>

#import "DataCenter+Cube.h"
#import "CubesModel.h"
#import "BSLCubeConstant.h"


typedef enum{
    MessageSecureTypeNone=0,
    MessageSecureTypeChangeRole,
    MessageSecureTypeChangePrivilege
}MessageSecureType;

@interface MessageManager()<ZillaDelegate>

-(void)startTimer:(BOOL)callNow;

-(void)sendFeedBack:(NSArray*)array;

-(MessageSecureType)fillMessageLists:(NSArray*)list outputNewList:(NSMutableArray*)newList;

-(void)syncFinish:(NSArray*)list;

@end

@implementation MessageManager{
    Zilla* zillaSDK;
    NSTimer* timer;
    
    CFURLRef soundFileURLRef;
    SystemSoundID soundFileObject;
    
    BOOL apnsNotify;
}


-(id)init{
    self=[super init];
    if(self){
        
        zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
        zillaSDK.delegate=self;

        NSURL *tapSound = [NSURL URLWithString:@"/System/Library/Audio/UISounds/sms-received1.caf"];
        soundFileURLRef = (CFURLRef) CFBridgingRetain(tapSound);
        AudioServicesCreateSystemSoundID (soundFileURLRef,&soundFileObject);
        
    }
    return self;
}

- (void)dealloc{
    [self cancel];
    CFRelease(soundFileURLRef);
}

-(void)cancel{
    [zillaSDK cancelAllWorkGroup];

    
    [timer invalidate];
    timer=nil;
}

-(void)syncUpdateNumber{
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    ChamleonResultSet* resultSet=[database executeQuery:@"select * from message"];
    
    MessageModel* messageModel=[[MessageModel alloc] init];
    while([resultSet next]){
        messageModel.isRead=[resultSet boolForColumn:@"isRead"];
        messageModel.moduleIdentifer=[resultSet stringForColumn:@"moduleIdentifer"];
        messageModel.moduleBadge=[resultSet intForColumn:@"moduleBadge"];
        if(!messageModel.isRead){
            if([messageModel.moduleIdentifer length]>0){
                CubeModel* cubeModel=[[DataCenter defaultCenter] finidServiceCubeModule:messageModel.moduleIdentifer];
                if([messageModel.moduleIdentifer isEqualToString:@"com.foss.message.record"] || [messageModel.moduleIdentifer isEqualToString:@"com.foss.message"])
                    cubeModel.unReadCount++;
                else{
                    cubeModel.moduleBadge = messageModel.moduleBadge;
                    if(cubeModel.moduleBadge){
                        cubeModel.unReadCount++;
                    }
                }
            }
        }
    }
    [resultSet close];
    [database close];
    
}

-(MessageModel*)messageById:(NSString*)messageId{
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    ChamleonResultSet* resultSet=[database executeQuery:[NSString stringWithFormat:@"select * from message where id='%@'",messageId]];
    
    MessageModel* messageModel=nil;
    if([resultSet next]){
        messageModel=[[MessageModel alloc] init];

        messageModel.messageId=[resultSet stringForColumn:@"id"];
        messageModel.title=[resultSet stringForColumn:@"title"];
        messageModel.content=[resultSet stringForColumn:@"content"];
        messageModel.messageType=[resultSet stringForColumn:@"messageType"];
        messageModel.receiveTime=[resultSet dateForColumn:@"receiveTime"];
        messageModel.isRead=[resultSet boolForColumn:@"isRead"];
        messageModel.moduleIdentifer=[resultSet stringForColumn:@"moduleIdentifer"];
        messageModel.moduleName=[resultSet stringForColumn:@"moduleName"];
        messageModel.announceId=[resultSet stringForColumn:@"announceId"];
        messageModel.moduleBadge=[resultSet intForColumn:@"moduleBadge"];
        messageModel.busiDetail=[resultSet intForColumn:@"busiDetail"];
        messageModel.moduleUrl=[resultSet stringForColumn:@"moduleUrl"];
    }
    [resultSet close];
    [database close];

    return messageModel;
}


-(void)syncMessage{
    [self cancel];
    apnsNotify=NO;
    [zillaSDK syncReceipts];
}

-(void)syncMessageById:(NSString*)messageId routing:(BOOL)routing{
    [self cancel];
    apnsNotify=routing;
    [zillaSDK syncReceiptsWithMessageId:messageId];
}

-(void)syncFinish:(NSArray*)list{
    if ([list count] > 0){
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
        MessageSecureType existSecurity=[self fillMessageLists:list outputNewList:array];
        if(existSecurity!=MessageSecureTypeNone){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Cube_SecurityUpdated" object:[NSNumber numberWithBool:(existSecurity==MessageSecureTypeChangeRole)]];
        }
        [self sendFeedBack:list];
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleUpdateIconNumber object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Cube_MessageUpdated" object:[NSNumber numberWithBool:YES]];
        
        if([array count]>0)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Cube_MessageNewInsert" object:array];
        
        if(apnsNotify){
            [self checkApnsRedirectAndRedirect:[list objectAtIndex:0] routingByMessageRecord:NO];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Cube_MessageUpdated" object:[NSNumber numberWithBool:NO]];

        [self startTimer:NO];
    }
}

-(void)checkApnsRedirectAndRedirect:(MessageModel*)messageModel routingByMessageRecord:(BOOL)routingByMessageRecord{
    
    NSString* moduleIdentifier=nil;
    
    NSString* pageIdentifier=nil;
    NSMutableDictionary* params=[[NSMutableDictionary alloc] initWithCapacity:2];
    
    if([messageModel.messageType isEqualToString:@"SYS"] || [messageModel.messageType isEqualToString:@"SECURITY"]){
        
        [params setObject:@"1" forKey:@"html5Module"];
        moduleIdentifier=@"Module";
        pageIdentifier=@"com.foss.message";

        /*
        moduleIdentifier=@"Module";
        pageIdentifier=@"MessageViewController";
        */
    }
    else{
        //判断该模块是否已经有安装 ，没有安装就直接跳去系统模块
        if(![[DataCenter defaultCenter] finidInstallCubeModule:messageModel.moduleIdentifer]){
            [params setObject:@"1" forKey:@"html5Module"];
            moduleIdentifier=@"Module";
            pageIdentifier=@"com.foss.message";
            
            /*
             moduleIdentifier=@"Module";
             pageIdentifier=@"MessageViewController";
             */
        }
        else{
            //先找出是否有该模块标识所隶属的原生工程模块，  如果没有则为html模块
            moduleIdentifier=[[RoutingParserHelper sharedRouting] moduleIdentifierForLink:messageModel.moduleUrl identifier:messageModel.moduleIdentifer];
            if(moduleIdentifier==nil){     //没有，则为html5 模块
                [params setObject:@"1" forKey:@"html5Module"];
                moduleIdentifier=@"Module";
                pageIdentifier=messageModel.moduleIdentifer;
                if([messageModel.moduleUrl length]>0)
                    [params setObject:messageModel.moduleUrl forKey:@"moduleUrl"];
                
            }
            else{
                NSString* moduleUrl=messageModel.moduleUrl;
                if([moduleUrl length]<1)
                    moduleUrl=@"/index";
                pageIdentifier=[[RoutingParserHelper sharedRouting] pageIdentifierForLink:moduleUrl identifier:messageModel.moduleIdentifer outParams:params];
                if([pageIdentifier length]<1){
                    [params removeAllObjects];
                    moduleUrl=@"/index";
                    pageIdentifier=[[RoutingParserHelper sharedRouting] pageIdentifierForLink:moduleUrl identifier:messageModel.moduleIdentifer outParams:params];
                }
            }
        }
    }
    
    if(routingByMessageRecord){
        [params setObject:[NSNumber numberWithBool:YES] forKey:@"routingByMessageRecord"];
    }
    
    NSDictionary* dict=[[NSDictionary alloc] initWithObjectsAndKeys:
                        moduleIdentifier,@"moduleIdentifier",
                        pageIdentifier,@"identifier",
                        params,@"params",
                        nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeAPNSPushModuleWeb object:dict];
    
}

-(NSArray*)messageGroupList{
    
    
    //    DataCenter* dataCenter=[DataCenter defaultCenter];
    
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=dataCenter.username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    
    ChamleonResultSet* resultSet=[database executeQuery:@"select moduleIdentifer,moduleName,isRead from Message"];
    
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
    while([resultSet next]){
        MessageGroupModel* model=nil;
        NSString* moduleIdentifer=[resultSet stringForColumnIndex:0];
        for(MessageGroupModel* _model in array){
            if([_model.moduleIdentifer isEqualToString:moduleIdentifer]){
                model=_model;
                break;
            }
        }
        if(model==nil){
            model=[[MessageGroupModel alloc] init];
            [array addObject:model];
        }
        model.moduleIdentifer=moduleIdentifer;
        model.moduleName=[resultSet stringForColumnIndex:1];
        BOOL isRead=[resultSet boolForColumnIndex:2];
        if(!isRead)
            model.unReadCount++;
        model.totalCount++;
        
    }
    [resultSet close];
    return array;
}

-(NSArray*)messageList:(NSString*)moduleIdentifier{
    
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:2];
    
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    ChamleonResultSet* resultSet=nil;
    if([moduleIdentifier length]>0)
        resultSet=[database executeQuery:[NSString stringWithFormat:@"select * from message where moduleIdentifer='%@' order by rowid desc",moduleIdentifier]];
    else
        resultSet=[database executeQuery:@"select * from message order by rowid desc"];

    [database setDateFormat:[ChamleonDB storeableDateFormat:@"yyyy-MM-dd hh:mm:ss +000"]];
    
    //2013-12-25 02:54:41 +0000
    while([resultSet next]){
        MessageModel* messageModel=[[MessageModel alloc] init];
        messageModel.messageId=[resultSet stringForColumn:@"id"];
        messageModel.title=[resultSet stringForColumn:@"title"];
        messageModel.content=[resultSet stringForColumn:@"content"];
        messageModel.messageType=[resultSet stringForColumn:@"messageType"];
        messageModel.receiveTime=[resultSet dateForColumn:@"receiveTime"];
        messageModel.isRead=[resultSet boolForColumn:@"isRead"];
        messageModel.moduleIdentifer=[resultSet stringForColumn:@"moduleIdentifer"];
        messageModel.moduleName=[resultSet stringForColumn:@"moduleName"];
        messageModel.announceId=[resultSet stringForColumn:@"announceId"];
        messageModel.moduleBadge=[resultSet intForColumn:@"moduleBadge"];
        messageModel.busiDetail=[resultSet intForColumn:@"busiDetail"];
        messageModel.moduleUrl=[resultSet stringForColumn:@"moduleUrl"];
        
        [array addObject:messageModel];
        
        //暂时先这样，到时加返load more
        if([array count]>=200 && [moduleIdentifier length]>0)break;
    }
    [resultSet close];
    [database close];
    
    return array;
}

-(void)removeMesageById:(NSString*)messageId{
    
    MessageModel* messageModel=[self messageById:messageId];
    
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    
    [database executeUpdate:[NSString stringWithFormat:@"delete from message where id='%@' ",messageModel.messageId]];
    [database close];
    
    if(!messageModel.isRead){
        CubeModel* cubeModel=[[DataCenter defaultCenter] finidServiceCubeModule:messageModel.moduleIdentifer];
        cubeModel.unReadCount--;
        if(cubeModel.unReadCount<1)
            cubeModel.unReadCount=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleUpdateIconNumber object:nil];
    }
}

-(void)removeMessageByGroup:(NSString*)moduleIdentifier{
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    [database executeUpdate:[NSString stringWithFormat:@"delete from message where moduleIdentifer='%@' ",moduleIdentifier]];
    [database close];
    
    CubeModel* cubeModel=[[DataCenter defaultCenter] finidServiceCubeModule:moduleIdentifier];
    cubeModel.unReadCount=0;
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleUpdateIconNumber object:nil];
    
}

-(void)setIsReadMessageById:(NSString*)messageId{
    
    MessageModel* messageModel=[self messageById:messageId];
    
    //    NSString* username=[DataCenter defaultCenter].username;
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    [database executeUpdate:[NSString stringWithFormat:@"update message set isRead=1 where id='%@' ",messageModel.messageId]];
    [database close];
    
    CubeModel* cubeModel=[[DataCenter defaultCenter] finidServiceCubeModule:messageModel.moduleIdentifer];
    cubeModel.unReadCount--;
    if(cubeModel.unReadCount<1)
        cubeModel.unReadCount=0;
    [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleUpdateIconNumber object:nil];
    
}

-(void)startTimer:(BOOL)callNow{
    [self cancel];
    timer=[NSTimer scheduledTimerWithTimeInterval:(callNow?3.0f:300.0f) target:self selector:@selector(syncMessage) userInfo:nil repeats:NO];
}

-(void)sendFeedBack:(NSArray*)array{
    [self cancel];
    NSMutableArray*  strs=[[NSMutableArray alloc] initWithCapacity:2];
    [array enumerateObjectsUsingBlock:^(id obj,NSUInteger index,BOOL*stop){
        MessageModel* model=obj;
        [strs addObject:model.messageId];
    }];
    [zillaSDK feedBackReceipt:strs];
}

-(MessageSecureType)fillMessageLists:(NSArray*)list outputNewList:(NSMutableArray*)newList{
    
    MessageSecureType existsSecurity=MessageSecureTypeNone;
    BOOL activateSound=NO;
    //    DataCenter* dataCenter=[DataCenter defaultCenter];
    
    DBStorageData* data=[[DBStorageHelper standandStorage] storageDataWithModuleIdentifier:@"Module"];
    //    data.identifier=dataCenter.username;
    ChamleonDB* database=[data openDatabase];
    [database open];
    
    NSDate* date=[NSDate date];
    [database beginTransaction];
    
    
    
    for(MessageModel* model in list){
        model.receiveTime=date;
        if ([model.messageType isEqualToString:@"SYS"])       //系统消息的话，提供一个identifier给它
        {
            model.moduleIdentifer = @"com.foss.message.record";
        }
        else if ([model.messageType isEqualToString:@"SECURITY"])       //权限更改的话，提供一个identifier给它
        {
            model.moduleIdentifer = @"com.foss.message.record";
        }
        @autoreleasepool {

            NSString* sql=[NSString stringWithFormat:@"insert into Message(id,title,content,messageType,receiveTime,isRead,moduleIdentifer,moduleName,announceId,moduleBadge,busiDetail,moduleUrl) values(?,?,?,?,datetime('now'),0,?,?,?,%d,%d,?)",model.moduleBadge,model.busiDetail];
            
            if([database executeUpdate:sql,model.messageId,model.title,model.content,model.messageType,model.moduleIdentifer,model.moduleName,model.announceId,model.moduleUrl]){
                activateSound=YES;
                if([model.moduleIdentifer length]>0){
                    CubeModel* cubeModel=[[DataCenter defaultCenter] finidServiceCubeModule:model.moduleIdentifer];
                    cubeModel.unReadCount++;
                    cubeModel.moduleBadge = model.moduleBadge;
                }
                if(!existsSecurity && [model.messageType isEqualToString:@"SECURITY"]){
                    existsSecurity=MessageSecureTypeChangePrivilege;
                    if([model.securityKey isEqualToString:@"privilege"]){// 如果是权限更新了
                        existsSecurity=MessageSecureTypeChangePrivilege;
                    }
                    else if([model.securityKey isEqualToString:@"roles"]){      //如果是角色更新了
                        existsSecurity=MessageSecureTypeChangeRole;
                    }
                }
                
                [newList addObject:model];
                
            }
        }
    }
    [database commit];
    [database close];
    
    if(activateSound)
        AudioServicesPlayAlertSound(soundFileObject);
    
    
    return existsSecurity;
}


#pragma mark zilla delegate

//未回执消息获取回调
-(void)syncReceiptsSuccess:(NSArray*)list{
    [self syncFinish:list];
}

-(void)syncReceiptsFailed:(int)statusCode errorMessage:(NSString*)errorMessage{
    [self syncFinish:nil];
}


//回执调用完成回调
-(void)feedBackCallBack{
    [self startTimer:YES];
}


@end
