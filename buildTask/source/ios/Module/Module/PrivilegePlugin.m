//
//  PrivilegePlugin.m
//  Module
//
//  Created by Fanty on 14-1-14.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "PrivilegePlugin.h"
#import "DataCenter+Cube.h"
#import <chameleon-ios-sdk/PriviligeModel.h>

@implementation PrivilegePlugin

-(void)getPrivileges:(CDVInvokedUrlCommand*)command{
    
    NSString* callbackId=command.callbackId;
    
    NSArray* priviliges=[DataCenter defaultCenter].rolePriviligeModel.priviliges;
    
    NSMutableString* str=[[NSMutableString alloc] initWithCapacity:1];
    [str appendString:@"["];
    for(int index=0;index<[priviliges count];index++){
        PriviligeModel* model=[priviliges objectAtIndex:index];
        [str appendFormat:@"\"%@\"",model.identifier];
        if(index<[priviliges count]-1)
            [str appendString:@","];
    }
    [str appendString:@"]"];

    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:str];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

}

@end
