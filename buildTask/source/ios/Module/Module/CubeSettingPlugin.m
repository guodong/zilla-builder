//
//  CubeSettingPlugin.m
//  Module
//
//  Created by Fanty on 14-1-3.
//  Copyright (c) 2014年 Fanty. All rights reserved.
//

#import "CubeSettingPlugin.h"
#import <chameleon-ios-sdk/ChamleonSDK.h>

#import "DataCenter+Cube.h"
#import "SVProgressHUD.h"

@interface CubeSettingPlugin()<ZillaDelegate,UIAlertViewDelegate>

@end

@implementation CubeSettingPlugin{
    Zilla* zillaSDK;
    
    NSString* callbackId;
}

-(void)getAppInfo:(CDVInvokedUrlCommand*)command{
    NSString* _callbackId=command.callbackId;
    
    ZillaUrlScheme* urlScheme=[[ZillaUrlScheme alloc] init];
    urlScheme.zillaAccessData=[ZillaAccessData activeSession];

    NSMutableString* json=[[NSMutableString alloc] initWithCapacity:5];
    [json appendString:@"{"];
    [json appendFormat:@"\"%@\":\"%@\",",@"appId",[[NSBundle mainBundle] bundleIdentifier]];
    [json appendFormat:@"\"%@\":\"%@\",",@"appKey",urlScheme.zillaAccessData.appKey];
    [json appendFormat:@"\"%@\":\"%@\",",@"sessionKey",urlScheme.zillaAccessData.appToken];
    
    
    [json appendFormat:@"\"%@\":\"%@\",",@"version",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    [json appendFormat:@"\"%@\":\"%@\",",@"deviceId",[[UIDevice currentDevice] uniqueDeviceIdentifier]];
    [json appendFormat:@"\"%@\":\"%@\",",@"appName",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];

    
    
    [json appendFormat:@"\"%@\":\"%@\",",@"loginUrl",[[urlScheme loginURL] absoluteString]];
    [json appendFormat:@"\"%@\":\"%@\"",@"logoutUrl",[[urlScheme logoutURL] absoluteString]];

    [json appendString:@"}"];


    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:json];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];
    
}

-(void)checkAppUpdate:(CDVInvokedUrlCommand*)command{
    if([callbackId length]>0){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    
    callbackId=command.callbackId;
    if(zillaSDK==nil){
        zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
        zillaSDK.delegate=self;
    }
    
    [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeBlack];
    [zillaSDK checkAppVersion];

}


#pragma mark zilla delegate
//应用版本回调
-(void)appVersionChecked:(AppInfoModel*)appInfoModel{
    [SVProgressHUD dismiss];
    int newBuild = appInfoModel.build;
    int currentBuild = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] intValue];
    if (newBuild > currentBuild){
        NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        [DataCenter defaultCenter].plist=appInfoModel.plist;
        NSString *message = [NSString stringWithFormat:@"当前版本:%@\n最新版本:%@\n版本说明:\n%@", currentVersion, appInfoModel.version, appInfoModel.releaseNote];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[ NSString stringWithFormat:@"%@平台版本更新",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] ] message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"更新", nil];
        alertView.tag=3;
        [alertView show];
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"暂未发现有版本更新" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
        
    }
    
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    callbackId=nil;
}

-(void)appVersionFailed:(int)statusCode errorMessage:(NSString*)errorMessage{
    if(statusCode>0){
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"更新出错，请检查网络" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"版本更新" message:@"暂未发现有版本更新" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [av show];
        av=nil;
        
    }
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    callbackId=nil;
}

#pragma mark alertview delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if( buttonIndex==1){
        NSString* downloadUrl=[zillaSDK.zillaUrlScheme attachmentByAIdWithNoAppKey:[DataCenter defaultCenter].plist];
        [[UIApplication sharedApplication] openURL:[zillaSDK.zillaUrlScheme applicationNewVersionLink:downloadUrl]];
    }
}


@end
