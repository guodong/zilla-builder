//
//  SecurityChangePlugin.m
//  Login
//
//  Created by Fanty on 13-12-26.
//  Copyright (c) 2013年 Fanty. All rights reserved.
//

#import "SecurityChangePlugin.h"

#import <chameleon-ios-sdk/ChamleonSDK.h>
#import "DataCenter+Cube.h"
#import <SVProgressHUD/SVProgressHUD.h>

#import "CubeModule.h"

@interface SecurityChangePlugin()<ZillaDelegate>

-(void)finish:(BOOL)success;
@end

@implementation SecurityChangePlugin{
    Zilla* zillaSDK;
    NSString* callbackId;
    NSString* accountName;
    
}

-(void)securityChange:(CDVInvokedUrlCommand*)command{
    if([callbackId length]>0){
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        return;
    }
    callbackId=command.callbackId;
    accountName =  [command.arguments objectAtIndex:0];
    
    [SVProgressHUD showWithStatus:@"正在获取帐户权限..." maskType:SVProgressHUDMaskTypeBlack];
    
    if(zillaSDK==nil){
        zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
        zillaSDK.delegate=self;
    }
    [zillaSDK cancelAllWorkGroup];
    
    [zillaSDK rolePriviligesList:accountName];
}

-(void)finish:(BOOL)success{
    CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:(success?CDVCommandStatus_OK:CDVCommandStatus_ERROR) messageAsString:@""];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
    callbackId=nil;
    accountName=nil;
}

#pragma mark  zilla sdk


-(void)rolePriviligesListSuccess:(RolePriviligeModel *)rolePriviligeModel{
    [SVProgressHUD dismiss];

    [DataCenter defaultCenter].accountToken=@"abcdefghijklmnopqrstuvwxyz";
    [DataCenter defaultCenter].username=accountName;
    [DataCenter defaultCenter].rolePriviligeModel=rolePriviligeModel;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeAuth_Success" object:[NSNumber numberWithBool:YES] ];
    CubeModule* cubeModule=(CubeModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Module"];
    [cubeModule.accountManager syncAuth:NO];

    
    [self finish:YES];

}

-(void)rolePriviligesListFailed:(int)statusCode errorMessage:(NSString *)errorMessage{
    [SVProgressHUD dismiss];

    if(statusCode==204 || statusCode==400){
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"应用没有为该帐户授权。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if(statusCode==500){
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"服务器返回异常，请稍候再试。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    else{
        UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"网络异常，请检查再试。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    [self finish:NO];
}

@end
