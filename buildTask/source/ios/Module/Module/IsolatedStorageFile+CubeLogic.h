//
//  CubeDataPathUtils.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import <Foundation/Foundation.h>
#import <MainModuleSDK/IsolatedStorageFile.h>

@class CubeModel;

@interface IsolatedStorageFile(CubeLogic)

//模块管理用户隔离目录
+(NSString*)localMoudlelistDirectory:(NSString*)account;

//本地映射目录
+(NSURL*)moduleMapInfoFile;

//下载的zip包临时文件
+(NSString*)cubeModelTempZip:(NSString*)identifier;

//模块存放目录
+(NSString*)cubeModuleIdentifierRoot:(NSString*)identifier;

//快照依赖文件
+(NSString*)landIconLocalFile:(NSString*)identifier;

//快照
+(NSString*)outputIcon:(CubeModel*)cubeModel;

//自动下载记录文件
+(NSString*)loadAutoSaveFile:(NSString*)account;


//www资源目录
+(NSString*)cubeResourceDirectory;

//www本地目录
+(NSString*)cordovaMainDirectory;


//依赖文件路径
+(NSString*)dependencieModuleFile:(NSString*)identifier;

//模块主页
+(NSString*)cubeModuleIdentifierMainPage:(NSString*)identifier;


@end
