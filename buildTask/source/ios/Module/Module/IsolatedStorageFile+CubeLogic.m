//
//  CubeDataPathUtils.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-3.
//
//

#import "IsolatedStorageFile+CubeLogic.h"
#import <chameleon-ios-sdk/ChamleonSDK.h>
#import <MainModuleSDK/MainUtils.h>
#import <MainModuleSDK/IsolatedStorageFile.h>
#import "CubesModel.h"
#import "DataCenter+Cube.h"
#import "CubeModule.h"




@implementation IsolatedStorageFile(CubeLogic)

+(NSString*)localMoudlelistDirectory:(NSString*)account{
    NSString* root=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    /*
    root=[root stringByAppendingPathComponent:account];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
     */
    
    root=[root stringByAppendingPathComponent:@"data"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"cube.json"];
    
    return root;
}

+(NSString*)loadAutoSaveFile:(NSString*)account{
    NSString* root=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    root=[root stringByAppendingPathComponent:account];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"data"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }
    
    root=[root stringByAppendingPathComponent:@"autosave.json"];
    
    return root;
    
}

+(NSURL*)moduleMapInfoFile{
    NSURL* url= [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Module.bundle/data/module_map_config.json" isDirectory:NO];
    return url;
}

+(NSString*)cubeModelTempZip:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    

    return [root stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",identifier]];
}

+(NSString*)cubeModuleIdentifierRoot:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];

    root=[root stringByAppendingPathComponent:identifier];
    if(![[NSFileManager defaultManager] fileExistsAtPath:root]){
        [[NSFileManager defaultManager] createDirectoryAtPath:root withIntermediateDirectories:YES attributes:Nil error:nil];
    }

    return root;
}

+(NSString*)landIconLocalFile:(NSString*)identifier{
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];

    return [root stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/icon.png",identifier]];
}

+(NSString*)outputIcon:(CubeModel *)cubeModel{
    if ([cubeModel.local length]>0){
        //本地模块，读取本地文件作为头像
        //return _icon;
        
        NSArray* moduleMaps=[DataCenter defaultCenter].moduleMaps;
        
        for (ModuleMap* moduleMap in moduleMaps){
            if ([moduleMap.identifier isEqualToString:cubeModel.identifier])
                return moduleMap.icon;
        }
        
        if([cubeModel.icon rangeOfString:@"http://"].length>0)
            return cubeModel.icon;
        else{
            ZillaUrlScheme* zillaUrlScheme=[[ZillaUrlScheme alloc] init];
            zillaUrlScheme.zillaAccessData=[ZillaAccessData activeSession];

            return [[zillaUrlScheme attachmentByIdURL:cubeModel.icon] absoluteString];
        }
    }
    else if (cubeModel.status == CubeMoudleStatusFinish || cubeModel.status==CubeMoudleStatusCanUpdate){
        //模块已安装，用文件夹中的头像
        //return _icon;
        
        NSString* image = [IsolatedStorageFile landIconLocalFile:cubeModel.identifier];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:image isDirectory:NO]){

            return image;
        }
        if([cubeModel.icon rangeOfString:@"http://"].length>0)
            return cubeModel.icon;
        else{
            ZillaUrlScheme* zillaUrlScheme=[[ZillaUrlScheme alloc] init];
            zillaUrlScheme.zillaAccessData=[ZillaAccessData activeSession];
            return [[zillaUrlScheme attachmentByIdURL:cubeModel.icon] absoluteString];
        }
    }
    else{
        if([cubeModel.icon rangeOfString:@"http://"].length>0)
            return cubeModel.icon;
        else{
            ZillaUrlScheme* zillaUrlScheme=[[ZillaUrlScheme alloc] init];
            zillaUrlScheme.zillaAccessData=[ZillaAccessData activeSession];
            return [[zillaUrlScheme attachmentByIdURL:cubeModel.icon] absoluteString];

        }
    }
}


+(NSString*)cubeResourceDirectory{
    NSURL* url= [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Module.bundle/www" isDirectory:YES];
    
    return [url path];
    
}

+(NSString*)cordovaMainDirectory{
    
    NSString* path=[[IsolatedStorageFile cordovaRootDirectory] stringByAppendingPathComponent:([MainUtils isPad]?@"pad/main.html":@"phone/index.html")];
    return path;
    
}

+(NSString*)dependencieModuleFile:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    
    root=[root stringByAppendingPathComponent:identifier];
    
    return [root stringByAppendingPathComponent:@"CubeModule.json"];
}

+(NSString*)cubeModuleIdentifierMainPage:(NSString*)identifier{
    
    NSString* root=[IsolatedStorageFile cordovaRootDirectory];
    
    root=[root stringByAppendingPathComponent:identifier];
    
    return [root stringByAppendingPathComponent:@"index.html"];
    
}


@end
