//
//  CubesModel.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubesModel.h"

@implementation DependsOnModule


@end

@implementation ModuleMap


@end

@implementation CubeModuleJsonSerial

-(id)init{
    self=[super init];
    if(self){
        NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:1];
        self.allKeys=array;
        
        array=[[NSMutableArray alloc] initWithCapacity:1];
        self.allValues=array;
    }
    
    return self;
}

- (void)dealloc{
    [self.allKeys removeAllObjects];
    [self.allValues removeAllObjects];
}

-(NSMutableArray*)objectForKey:(NSString*)key{
    for(int index=0;index<[self.allKeys count];index++){
        NSString* _key=[self.allKeys objectAtIndex:index];
        if([_key isEqualToString:key]){
            return [self.allValues objectAtIndex:index];
        }
    }
    return nil;
}

-(void)setObject:(NSMutableArray*)list forKey:(NSString*)key{
    for(int index=0;index<[self.allKeys count];index++){
        NSString* _key=[self.allKeys objectAtIndex:index];
        if([_key isEqualToString:key]){
            return;
        }
    }
    
    [self.allKeys addObject:key];
    [self.allValues addObject:list];
}


@end


@implementation MessageGroupModel

@end


