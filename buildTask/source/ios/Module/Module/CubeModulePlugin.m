//
//  CubeModulePlugin.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-2.
//
//

#import "CubeModulePlugin.h"
#import "CubesModel.h"

#import "DataCenter+Cube.h"
#import "CubeModuleParser.h"
#import "BSLCubeConstant.h"

@implementation CubeModulePlugin



-(void)mainList:(CDVInvokedUrlCommand*)command{
    @autoreleasepool {
        NSString* callbackId=command.callbackId;
        //CubeModuleJsonSerial* models = [[DataCenter defaultCenter] mainListCategroyMap];
        NSDictionary* models = [[DataCenter defaultCenter] mainListCategroyMap];

        NSString* result=[CubeModuleParser handleModuleToJson:models];
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CubeModuleUpdateIconNumber object:nil];
    }
}

-(void)uninstallList:(CDVInvokedUrlCommand*)command{
    @autoreleasepool {
        NSString* callbackId=command.callbackId;
//        CubeModuleJsonSerial* models = [[DataCenter defaultCenter] uninstallCategroyMap];
        NSDictionary* models = [[DataCenter defaultCenter] uninstallCategroyMap];

        NSString* result=[CubeModuleParser handleModuleToJson:models];
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }
}

-(void)installList:(CDVInvokedUrlCommand*)command{
    @autoreleasepool {
        NSString* callbackId=command.callbackId;
//        CubeModuleJsonSerial* models = [[DataCenter defaultCenter] installCategroyMap];
        NSDictionary* models = [[DataCenter defaultCenter] installCategroyMap];

        NSString* result=[CubeModuleParser handleModuleToJson:models];
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }

}

-(void)upgradableList:(CDVInvokedUrlCommand*)command{
    @autoreleasepool {
        NSString* callbackId=command.callbackId;
//        CubeModuleJsonSerial* models = [[DataCenter defaultCenter] updatableCategroyMap];
        NSDictionary* models = [[DataCenter defaultCenter] updatableCategroyMap];

        NSString* result=[CubeModuleParser handleModuleToJson:models];
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    }

}


-(void)ShowMainView:(CDVInvokedUrlCommand*)command{
    NSLog(@"showMainView");
}


@end










