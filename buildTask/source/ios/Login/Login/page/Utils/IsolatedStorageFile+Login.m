//
//  LoginPathUtils.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-28.
//
//

#import "IsolatedStorageFile+Login.h"

#import <chameleon-ios-sdk/CApplication.h>
#import <MainModuleSDK/MainUtils.h>
#import <MainModuleSDK/MainModule.h>

@implementation IsolatedStorageFile(Login)

+(NSString*)loginResourceDirectory{
    NSURL* url= [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Login.bundle/www" isDirectory:YES];
    
    return [url path];

}

+(NSString*)cordovaLoginDirectory{
    NSString* path=[[IsolatedStorageFile cordovaRootDirectory] stringByAppendingPathComponent:([MainUtils isPad]?@"pad/login.html":@"phone/login.html")];
    return path;
    
    return @"";
}


@end
