//
//  LoginPlugin.m
//  bsl-sdk
//
//  Created by Fanty on 13-11-29.
//
//

#import "LoginPlugin.h"

#import <MainModuleSDK/DataCenter.h>
#import <chameleon-ios-sdk/ChamleonSDK.h>
#import "LoginModule.h"

@interface LoginPlugin()
-(void)saveUserInfo:(NSString*)username password:(NSString*)password isRemember:(BOOL)isRemember accountToken:(NSString*)accountToken;
@end

@implementation LoginPlugin

-(void)getAccountMessage:(CDVInvokedUrlCommand*)command{
    @autoreleasepool {
        
        
        NSString* username = [DataCenter defaultCenter].username;
        NSString* password = [DataCenter defaultCenter].password;
        BOOL isRemember = [DataCenter defaultCenter].isRemember;
        NSString* result=nil;
        if ([username length]<1 || [password length]<1){
            result = @"{\"username\":\"\", \"password\":\"\", \"isRemember\" : false}";
        }
        else
        {
            result=[NSString stringWithFormat:@"{\"username\":\"%@\",\"password\":\"%@\",\"isRemember\":%@}",username,password,(isRemember?@"true":@"false")];
        }
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }
    
    
}

-(void)login:(CDVInvokedUrlCommand*)command{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeLogin_Start" object:nil];

    NSString* callbackId=command.callbackId;
    NSString* name =  [command.arguments objectAtIndex:0];
    NSString* password =  [command.arguments objectAtIndex:1];
    NSString* isRemeber =  [command.arguments objectAtIndex:2];
        

    Zilla* zillaSDK=[[Zilla alloc] initWithZillAccessData:[ZillaAccessData activeSession]];
    
    AsyncRequestWorkGroup* task= [zillaSDK loginWithWorkGroup:name password:password];
    __block AsyncRequestWorkGroup* __task=task;
    __block LoginPlugin* __loginPlugin=self;
    [task setFinishBlock:^{
    
        NSString* resultMessage=nil;
        NSString* alertMessage=nil;
        CDVCommandStatus status=CDVCommandStatus_ERROR;
        if([__task result]!=nil){
            LoginModel* loginModel=(LoginModel*)[__task result];
            if(!loginModel.hasOperation){//  没有权限
                alertMessage=@"用户没有操作权限请联系管理员";
                resultMessage = @"{\"isSuccess\":false,\"message\":\"用户没有操作权限请联系管理员\"}";

            }
            else{
                [__loginPlugin saveUserInfo:name password:password isRemember:([isRemeber isEqualToString:@"true"]) accountToken:loginModel.accountToken];
                
                resultMessage = @"{\"isSuccess\":true,\"message\":\"登录成功\"}";
                status=CDVCommandStatus_OK;
            }
        }
        else{
            alertMessage=[__task errorMessage];

            resultMessage = [NSString stringWithFormat:@"{\"isSuccess\":false,\"message\":\"%@\"}",[__task errorMessage]];
        }
        
        if(status==CDVCommandStatus_OK){
            NSDictionary* dict=[NSDictionary dictionaryWithObjectsAndKeys:name,@"name",[DataCenter defaultCenter].accountToken,@"accountToken", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeLogin_Success" object:dict];
        }
        else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CubeLogin_Failed" object:alertMessage];
        }
        CDVPluginResult*pluginResult = [CDVPluginResult resultWithStatus:status messageAsString:resultMessage];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];

    }];
    
}

-(void)saveUserInfo:(NSString*)username password:(NSString*)password isRemember:(BOOL)isRemember accountToken:(NSString*)accountToken{
    DataCenter* dataCenter=[DataCenter defaultCenter];
    dataCenter.username=username;
    dataCenter.password=password;
    dataCenter.isRemember=isRemember;
    dataCenter.accountToken=accountToken;
    LoginModule* loginModule=(LoginModule*)[[CApplication sharedApplication] moduleForIdentifier:@"Login"];
    [loginModule.loginManager saveAccount];
}


@end
