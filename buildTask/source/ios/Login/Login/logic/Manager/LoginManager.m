//
//  LoginManager.m
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import "LoginManager.h"
#import <MainModuleSDK/DataCenter.h>
#import <chameleon-ios-sdk/ChamleonSDK.h>


@interface LoginManager()
@end

@implementation LoginManager

-(id)init{
    self=[super init];
    if(self){
    }
    return self;
}

-(void)restoreAccount{
    NSUserDefaults* userDefault=[NSUserDefaults standardUserDefaults];
    
    [DataCenter defaultCenter].username=[userDefault objectForKey:@"loginModule_username"];
    [DataCenter defaultCenter].password=[userDefault objectForKey:@"loginModule_password"];
    [DataCenter defaultCenter].isRemember=([[userDefault objectForKey:@"loginModule_isRemember"] isEqualToString:@"true"]);
    
}

-(void)saveAccount{
    NSUserDefaults* userDefault=[NSUserDefaults standardUserDefaults];
    
    [userDefault setObject:[DataCenter defaultCenter].username forKey:@"loginModule_username"];
    [userDefault setObject:[DataCenter defaultCenter].password forKey:@"loginModule_password"];
    [userDefault setObject:([DataCenter defaultCenter].isRemember?@"true":@"false") forKey:@"loginModule_isRemember"];
    [userDefault synchronize];
}

@end
