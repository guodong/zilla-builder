//
//  LoginManager.h
//  bsl-sdk
//
//  Created by Fanty on 13-12-1.
//
//

#import <Foundation/Foundation.h>

@class AsyncRequestWorkGroup;
@class Zilla;

@interface LoginManager : NSObject{
    Zilla* zillaSDK;
}

//获取已登陆的帐号信息
-(void)restoreAccount;

//保存登陆帐号
-(void)saveAccount;
@end
