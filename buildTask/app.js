/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var build = require('./build/buildTask');
var progress = require('./build/buildProgress');
var downfile = require('./build/filedown');

var Task = require('./models/buildTask.js');


var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.post('/build-task/:id', build.index);
app.get('/build-task/:id', build.index);
app.get('/build-progress/:id', progress.index);
app.get('/attachment/:type/:id', downfile.index);

app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function() {
	//打包服务重启时     将所有任务至于失败 
	Task.UpdateBuilding(function(err, builds) {
	});
	console.log('Express server listening on port ' + app.get('port'));
});