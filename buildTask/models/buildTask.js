var mongodb = require('./db');

function Task(_task) {
	this.task_id = _task.task_id;
	this.identifier = _task.identifier;
	this.version = _task.version;
	this.build = _task.build;
	this.appName= _task.appName;
	this.appkey = _task.appkey;
	this.certificate = _task.certificate;
	this.secretKey = _task.secretKey;
	this.provision = _task.provision;
	this.statue = _task.statue; /*1.排队   2.打包中  3.打包成功  4.打包失败 */
	this.createTime = _task.createTime;
	this.finishTime = _task.finishTime;
	this.buildInfo = _task.buildInfo;
	this.platform = _task.platform;
	this.buildLog = _task.buildLog;
	this.msg = _task.msg;
	this.ipaFilePath = _task.ipaFilePath;
	this.shameName = _task.shameName;
}


module.exports = Task;

Task.prototype.save = function(callback) {
	var buildTask = {
		shameName: this.shameName,
		appName: this.appName,
		ipaFilePath: this.ipaFilePath,
		task_id: this.task_id,
		identifier: this.identifier,
		version: this.version,
		build: this.build,
		appkey: this.appkey,
		certificate: this.certificate,
		secretKey: this.secretKey,
		provision: this.provision,
		statue: this.statue,
		createTime: this.createTime,
		finishTime: this.finishTime,
		buildInfo: this.buildInfo,
		platform: this.platform,
		buildLog: this.buildLog,
		msg: this.msg
	};

	mongodb.open(function(err, db) {
		if (err) {
			return callback(err); //错误，返回 err 信息
		}

		db.collection('tasks', function(err, collection) {
			if (err) {
				mongodb.close();
				return callback(err); //错误，返回 err 信息
			}
			//将用户数据插入 tasks 集合
			collection.insert(buildTask, {
				safe: true
			}, function(err, buildTask) {
				mongodb.close();
				if (err) {
					return callback(err); //错误，返回 err 信息
				}
				callback(null, buildTask); //成功！err 为 null，并返回存储后的用户文档
			});
		});
	});
};
//获取正在打包的任务
Task.UpdateBuilding = function(callback) {
	mongodb.open(function(err, db) {
		if (err) {
			return callback(err); //错误，返回 err 信息
		}
		//读取 tasks 集合
		db.collection('tasks', function(err, collection) {
			if (err) {
				mongodb.close();
				return callback(err); //错误，返回 err 信息
			}
			collection.update({
				$or: [{
					statue: '1'
				}, {
					statue: '2'
				}]
			}, {
				$set: {
					statue: '4',
					buildInfo: '服务器重启'
				}
			}, function(err, docs) {
				mongodb.close();
				if (err) {
					return callback(err); //失败！返回 err 信息
				}
				callback(null, docs); //成功！返回查询的用户信息
			});
		});
	});
};

//读取任务状态信息
Task.get = function(task_id, callback) {
	//打开数据库
	mongodb.open(function(err, db) {
		if (err) {
			return callback(err); //错误，返回 err 信息
		}
		//读取 tasks 集合
		db.collection('tasks', function(err, collection) {
			if (err) {
				mongodb.close();
				return callback(err); //错误，返回 err 信息
			}
			//查找用户名（task_id键）值为 task_id 一个文档
			collection.findOne({
				task_id: task_id
			}, function(err, buildTask) {
				mongodb.close();
				if (err) {
					return callback(err); //失败！返回 err 信息
				}
				callback(null, buildTask); //成功！返回查询的用户信息
			});
		});
	});
};

//更新任务信息
Task.update = function(_task, callback) {
	mongodb.open(function(err, db) {
		if (err) {
			return callback(err);
		};
		db.collection('tasks', function(err1, collection) {
			if (err1) {
				mongodb.close();
				return callback(err1); //错误，返回 err 信息
			}
			collection.update({
				task_id: _task.task_id
			}, {
				$set: {
					statue: _task.statue,
					finishTime: _task.finishTime,
					buildInfo: _task.buildInfo,
					buildLog: _task.buildLog,
					ipaFilePath: _task.ipaFilePath
				}
			}, function(error, bars) {
				mongodb.close();
				callback(error, bars);
			});
		});
	});
}